webpackJsonp([23],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGeofencePageModule", function() { return AddGeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_geofence__ = __webpack_require__(600);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__ = __webpack_require__(579);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddGeofencePageModule = /** @class */ (function () {
    function AddGeofencePageModule() {
    }
    AddGeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */]),
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__["a" /* NativeGeocoder */]
            ]
        })
    ], AddGeofencePageModule);
    return AddGeofencePageModule;
}());

//# sourceMappingURL=add-geofence.module.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NativeGeocoder; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(17);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Native Geocoder
 * @description
 * Cordova plugin for native forward and reverse geocoding
 *
 * @usage
 * ```typescript
 * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
 *
 * constructor(private nativeGeocoder: NativeGeocoder) { }
 *
 * ...
 *
 * let options: NativeGeocoderOptions = {
 *     useLocale: true,
 *     maxResults: 5
 * };
 *
 * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
 *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
 *   .catch((error: any) => console.log(error));
 *
 * this.nativeGeocoder.forwardGeocode('Berlin', options)
 *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
 *   .catch((error: any) => console.log(error));
 * ```
 * @interfaces
 * NativeGeocoderReverseResult
 * NativeGeocoderForwardResult
 * NativeGeocoderOptions
 */
var NativeGeocoder = (function (_super) {
    __extends(NativeGeocoder, _super);
    function NativeGeocoder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Reverse geocode a given latitude and longitude to find location address
     * @param latitude {number} The latitude
     * @param longitude {number} The longitude
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderReverseResult[]>}
     */
    /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    NativeGeocoder.prototype.reverseGeocode = /**
       * Reverse geocode a given latitude and longitude to find location address
       * @param latitude {number} The latitude
       * @param longitude {number} The longitude
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderReverseResult[]>}
       */
    function (latitude, longitude, options) { return; };
    /**
     * Forward geocode a given address to find coordinates
     * @param addressString {string} The address to be geocoded
     * @param options {NativeGeocoderOptions} The options
     * @return {Promise<NativeGeocoderForwardResult[]>}
     */
    /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    NativeGeocoder.prototype.forwardGeocode = /**
       * Forward geocode a given address to find coordinates
       * @param addressString {string} The address to be geocoded
       * @param options {NativeGeocoderOptions} The options
       * @return {Promise<NativeGeocoderForwardResult[]>}
       */
    function (addressString, options) { return; };
    NativeGeocoder.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Number, Number, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "reverseGeocode", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Object]),
        __metadata("design:returntype", Promise)
    ], NativeGeocoder.prototype, "forwardGeocode", null);
    /**
     * @name Native Geocoder
     * @description
     * Cordova plugin for native forward and reverse geocoding
     *
     * @usage
     * ```typescript
     * import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     *
     * constructor(private nativeGeocoder: NativeGeocoder) { }
     *
     * ...
     *
     * let options: NativeGeocoderOptions = {
     *     useLocale: true,
     *     maxResults: 5
     * };
     *
     * this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, options)
     *   .then((result: NativeGeocoderReverseResult[]) => console.log(JSON.stringify(result[0])))
     *   .catch((error: any) => console.log(error));
     *
     * this.nativeGeocoder.forwardGeocode('Berlin', options)
     *   .then((coordinates: NativeGeocoderForwardResult[]) => console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude))
     *   .catch((error: any) => console.log(error));
     * ```
     * @interfaces
     * NativeGeocoderReverseResult
     * NativeGeocoderForwardResult
     * NativeGeocoderOptions
     */
    NativeGeocoder = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["h" /* Plugin */])({
            pluginName: 'NativeGeocoder',
            plugin: 'cordova-plugin-nativegeocoder',
            pluginRef: 'nativegeocoder',
            repo: 'https://github.com/sebastianbaar/cordova-plugin-nativegeocoder',
            platforms: ['iOS', 'Android']
        })
    ], NativeGeocoder);
    return NativeGeocoder;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddGeofencePage = /** @class */ (function () {
    function AddGeofencePage(apiCall, alertCtrl, toastCtrl, fb, navCtrl, googleMaps, geoLocation, viewCtrl, nativeGeocoder, events, plt) {
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.googleMaps = googleMaps;
        this.geoLocation = geoLocation;
        this.viewCtrl = viewCtrl;
        this.nativeGeocoder = nativeGeocoder;
        this.events = events;
        this.plt = plt;
        this.finalcordinate = [];
        this.cord = [];
        this.autocompleteItems = [];
        this.autocomplete = {};
        this.storedLatLng = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.geofenceForm = fb.group({
            geofence_name: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            check: [false],
            check1: [false]
        });
        this.acService = new google.maps.places.AutocompleteService();
    }
    AddGeofencePage.prototype.ngOnInit = function () {
        // this.runGeofenceMaps();
        this.drawGeofence();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    };
    AddGeofencePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddGeofencePage.prototype.enteringFunc = function () {
        console.log(this.geofenceForm.value.check);
        this.entering = this.geofenceForm.value.check;
    };
    AddGeofencePage.prototype.exitingFunc = function () {
        console.log(this.geofenceForm.value.check1);
        this.exiting = this.geofenceForm.value.check1;
    };
    AddGeofencePage.prototype.creategoefence = function () {
        var _this = this;
        var that = this;
        that.submitAttempt = true;
        for (var r = 0; r < that.storedLatLng.length; r++) {
            var a = [];
            a.push(parseFloat(that.storedLatLng[r].lng));
            a.push(parseFloat(that.storedLatLng[r].lat));
            that.cord.push(a);
        }
        that.finalcordinate.push(that.cord);
        if (this.geofenceForm.valid) {
            if (this.entering || this.exiting) {
                // if (this.geofenceForm.value.geofence_name && this.finalcordinate.length) {
                if (this.geofenceForm.value.geofence_name && that.finalcordinate.length) {
                    // console.log("add device click")
                    // console.log(this.islogin._id);
                    var data = {
                        "uid": this.islogin._id,
                        "geoname": this.geofenceForm.value.geofence_name,
                        "entering": this.entering,
                        "exiting": this.exiting,
                        // "geofence": this.finalcordinate
                        "geofence": that.finalcordinate
                    };
                    this.apiCall.startLoading().present();
                    this.apiCall.addgeofenceCall(data)
                        .subscribe(function (data) {
                        _this.apiCall.stopLoading();
                        _this.devicesadd = data;
                        console.log(_this.devicesadd);
                        var toast = _this.toastCtrl.create({
                            message: 'Created geofence successfully',
                            position: 'bottom',
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            console.log('Dismissed toast');
                            // this.navCtrl.push(GeofencePage);
                            // this.viewCtrl.dismiss();
                            _this.events.publish('reloadDetails');
                            _this.navCtrl.pop();
                        });
                        toast.present();
                    }, function (err) {
                        _this.apiCall.stopLoading();
                        var alert = _this.alertCtrl.create({
                            message: 'Please draw valid geofence..',
                            buttons: [{
                                    text: 'OK', handler: function () {
                                        that.storedLatLng = [];
                                        that.finalcordinate = [];
                                        that.cord = [];
                                        _this.drawGeofence();
                                    }
                                }]
                        });
                        alert.present();
                        console.log(err);
                    });
                }
                else {
                    var toast = this.toastCtrl.create({
                        message: 'Select Geofence On Map!',
                        position: 'top',
                        duration: 2000
                    });
                    toast.present();
                }
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    message: 'All fields are required!',
                    buttons: ['Try Again']
                });
                alert_1.present();
            }
        }
    };
    AddGeofencePage.prototype.updateSearch = function () {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var that = this;
        var config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);
            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems);
        });
    };
    AddGeofencePage.prototype.chooseItem = function (item) {
        var _this = this;
        var that = this;
        that.autocomplete.query = item.description;
        console.log("console items=> " + JSON.stringify(item));
        that.autocompleteItems = [];
        var options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then(function (coordinates) {
            console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
            that.newLat = coordinates[0].latitude;
            that.newLng = coordinates[0].longitude;
            var pos = {
                target: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
                zoom: 15,
                tilt: 30
            };
            _this.map.moveCamera(pos);
            _this.map.addMarker({
                title: '',
                position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
            }).then(function (data) {
                console.log("Marker added");
            });
        })
            .catch(function (error) { return console.log(error); });
    };
    AddGeofencePage.prototype.drawGeofence = function () {
        var _this = this;
        if (this.map != undefined) {
            this.map.remove();
        }
        this.mapElement = document.getElementById('mapGeofence');
        console.log(this.mapElement);
        this.map = this.googleMaps.create(this.mapElement);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            console.log('Map is ready!');
            _this.geoLocation.getCurrentPosition().then(function (resp) {
                var pos = {
                    target: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](resp.coords.latitude, resp.coords.longitude),
                    zoom: 18,
                    tilt: 30
                };
                _this.map.moveCamera(pos);
                _this.map.addMarker({
                    title: '',
                    position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](resp.coords.latitude, resp.coords.longitude),
                }).then(function (data) {
                    console.log("Marker added");
                });
            });
            var that = _this;
            that.map.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MAP_CLICK).subscribe(function (data) {
                that.storedLatLng.push(data[0]);
                var urlString = '';
                if (that.plt.is('ios')) {
                    urlString = 'www/assets/imgs/circle1.png';
                }
                else {
                    if (that.plt.is('android')) {
                        urlString = './assets/imgs/circle1.png';
                    }
                    else {
                        urlString = './assets/imgs/circle1.png';
                    }
                }
                that.map.addMarker({
                    position: data[0],
                    icon: './assets/imgs/circle1.png'
                }).then(function (mark) {
                    console.log("Marker added");
                    mark.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (latLng) {
                        that.storedLatLng.push(that.storedLatLng[0]); // store first lat lng at last also
                        var GORYOKAKU_POINTS = that.storedLatLng;
                        that.map.addPolygon({
                            'points': GORYOKAKU_POINTS,
                            'strokeColor': '#000',
                            'fillColor': '#ffff00',
                            'strokeWidth': 5
                        }).then(function (polygon) {
                            console.log("GORYOKAKU_POINTS=> " + JSON.stringify(GORYOKAKU_POINTS));
                            // this.disableTap()
                        });
                    });
                });
                console.log(JSON.stringify(that.storedLatLng));
                if (that.storedLatLng.length > 1) {
                    that.map.addPolyline({
                        points: that.storedLatLng,
                        color: '#000000',
                        width: 3,
                        geodesic: true
                    });
                }
            });
        });
    };
    AddGeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-add-geofence',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/geofence/add-geofence/add-geofence.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Geofence</ion-title>\n\n        <!-- <ion-buttons end>\n\n            <button ion-button ion-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <!-- <ion-item class="item item-input controls">\n\n        <ion-icon name="search" item-start></ion-icon>\n\n        <input item-end id="pac-input" type="text" placeholder="Search Location" data-tap-disabled="true" (ionChange)=\'disableTap()\'\n\n            [(ngModel)]="search">\n\n    </ion-item> -->\n\n    <!-- <ion-list>\n\n        <ion-item>\n\n            <ion-input (click)="showAddressModal()" [(ngModel)]="address.place" type="text" placeholder="Pick an address"> </ion-input>\n\n        </ion-item>\n\n    </ion-list> -->\n\n    <!-- <div class="pac-container"> -->\n\n        <div #mapGeofence id="mapGeofence" data-tap-disabled="true">\n\n            <ion-searchbar class="search_bar" [(ngModel)]="autocomplete.query" (ionInput)="updateSearch()" (ionCancel)="onCancel($event)"\n\n                placeholder="Search Places...">\n\n            </ion-searchbar>\n\n            <ion-list>\n\n                <ion-item *ngFor="let item of autocompleteItems" (click)="chooseItem(item)">\n\n                    {{ item.description }}\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n    <!-- </div> -->\n\n    <!-- <div #mapGeofence id="mapGeofence" data-tap-disabled="true"></div> -->\n\n\n\n    <form [formGroup]="geofenceForm">\n\n        <ion-item class="logitem">\n\n            <ion-input formControlName="geofence_name" type="text" placeholder="GeoFence Name *"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!geofenceForm.controls.geofence_name.valid && (geofenceForm.controls.geofence_name.dirty || submitAttempt)">\n\n            <p>geofence_name required!</p>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Entering Geo Fence</ion-label>\n\n            <ion-checkbox color="dark" formControlName="check" (ionChange)="enteringFunc()"></ion-checkbox>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Exiting Geo Fence</ion-label>\n\n            <ion-checkbox color="dark" formControlName="check1" (ionChange)="exitingFunc()"></ion-checkbox>\n\n        </ion-item>\n\n    </form>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center">\n\n                <button ion-button clear color="light" (click)="creategoefence()">Create Geofence</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/geofence/add-geofence/add-geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMaps */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], AddGeofencePage);
    return AddGeofencePage;
}());

//# sourceMappingURL=add-geofence.js.map

/***/ })

});
//# sourceMappingURL=23.js.map