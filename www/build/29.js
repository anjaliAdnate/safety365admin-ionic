webpackJsonp([29],{

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(610);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 610:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { SignupPage } from '../signup/signup';

// import { DashboardPage } from '../dashboard/dashboard';
// import { MobileVerify } from './mobile-verify';
var LoginPage = /** @class */ (function () {
    // loading: any;
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl, events
        // public loadingCtrl: LoadingController
    ) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.showPassword = false;
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.loginForm.value.username == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.loginForm.value.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.loginForm.value.username);
        var isName = isNaN(this.loginForm.value.username);
        if (isEmail == false && isName == false) {
            this.data = {
                "psd": this.loginForm.value.password,
                "ph_num": this.loginForm.value.username
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.loginForm.value.password,
                "emailid": this.loginForm.value.username
            };
        }
        else {
            this.data = {
                "psd": this.loginForm.value.password,
                "user_id": this.loginForm.value.username
            };
        }
        this.apiservice.startLoading();
        this.apiservice.loginApi(this.data)
            .subscribe(function (response) {
            _this.logindata = response;
            _this.events.publish("auth:token", response.token);
            localStorage.setItem("AuthToken", response.token);
            _this.logindata = JSON.stringify(response);
            var logindetails = JSON.parse(_this.logindata);
            _this.userDetails = window.atob(logindetails.token.split('.')[1]);
            _this.details = JSON.parse(_this.userDetails);
            console.log(_this.details.email);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(_this.details));
            localStorage.setItem('condition_chk', _this.details.isDealer);
            _this.apiservice.stopLoading();
            var toast = _this.toastCtrl.create({
                message: "Welcome! You're logged In successfully.",
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.setRoot('DashboardPage');
            });
            toast.present();
            _this.responseMessage = "LoggedIn  successfully";
        }, function (error) {
            // console.log("login error => "+error)
            var body = error._body;
            var msg = JSON.parse(body);
            if (msg.message == "Mobile Phone Not Verified") {
                var confirmPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: 'Ok',
                            handler: function () {
                                // this.navCtrl.push(MobileVerify);
                            }
                        }
                    ]
                });
                confirmPopup.present();
            }
            else {
                // Do something on error
                var alertPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alertPopup.present();
            }
            _this.responseMessage = "Something Wrong";
            _this.apiservice.stopLoading();
        });
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.forgotPassFunc = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Forgot Password',
            message: "Enter your Registered  Mobile Number we will send you a confirmation code",
            inputs: [
                {
                    name: 'mobno',
                    placeholder: 'Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'SEND CONFIRMATION CODE',
                    handler: function (data) {
                        // console.log('Saved clicked');
                        // console.log(data.mobno);
                        var forgotdata = {
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassApi(forgotdata)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.otpMess = data;
                            console.log(_this.otpMess);
                            _this.PassWordConfirmPopup();
                            _this.responseMessage = "sent code successfully";
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: "Forgot Password Failed!",
                                message: msg.message,
                                buttons: ['OK']
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.PassWordConfirmPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Reset Password',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'newpass',
                    placeholder: 'Password'
                },
            ],
            buttons: [
                {
                    text: 'Back',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'SAVE',
                    handler: function (data) {
                        console.log('Saved clicked');
                        if (!data.newpass || !data.mobilenum || !data.confCode) {
                            var alertPopup = _this.alertCtrl.create({
                                title: 'Warning!',
                                message: "Fill all mandatory fields!",
                                buttons: ['OK']
                            });
                            alertPopup.present();
                        }
                        else {
                            if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                                if (data.newpass.length < 6 || data.newpass.length > 12) {
                                    var Popup = _this.alertCtrl.create({
                                        title: 'Warning!',
                                        message: "Password length should be 6 - 12",
                                        buttons: ['OK']
                                    });
                                    Popup.present();
                                }
                                else {
                                    var Passwordset = {
                                        "newpwd": data.newpass,
                                        "otp": data.confCode,
                                        "phone": _this.otpMess,
                                        "cred": _this.otpMess
                                    };
                                    _this.apiservice.startLoading();
                                    _this.apiservice.forgotPassMobApi(Passwordset)
                                        .subscribe(function (data) {
                                        _this.apiservice.stopLoading();
                                        _this.presentToast(data.message);
                                        _this.navCtrl.setRoot(LoginPage_1);
                                        _this.responseMessage = "password changed successfully";
                                    }, function (error) {
                                        _this.apiservice.stopLoading();
                                        var body = error._body;
                                        var msg = JSON.parse(body);
                                        var alert = _this.alertCtrl.create({
                                            title: "Forgot Password failed!",
                                            message: msg.message,
                                            buttons: ['OK']
                                        });
                                        alert.present();
                                        _this.responseMessage = "Something Wrong";
                                    });
                                }
                            }
                            else {
                                var alertPopup = _this.alertCtrl.create({
                                    title: 'Warning!',
                                    message: "New Password and Confirm Password Not Matched",
                                    buttons: ['OK']
                                });
                                alertPopup.present();
                            }
                            if (!data.newpass || !data.mobilenum || !data.confCode) {
                                //don't allow the user to close unless he enters model...
                                return false;
                            }
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/login/login.html"*/'<ion-content id=outer>\n  <div class="warning">\n    <!-- <div id=inner> -->\n    <div class="logo">\n      <img src="assets/imgs/icon.png">\n    </div>\n\n    <form [formGroup]="loginForm">\n      <div class="temp">\n        <ion-item class="logitem">\n          <ion-input formControlName="username" type="text" placeholder="Email/Mobile*"></ion-input>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n        <p>username required!</p>\n      </ion-item>\n      <div class="temp">\n        <ion-item class="logitem">\n          <ion-input formControlName="password" *ngIf="!showPassword" type="password" placeholder="Password*"></ion-input>\n          <ion-input formControlName="password" *ngIf="showPassword" type="text" placeholder="Password*"></ion-input>\n\n          <button ion-button clear item-end (click)="toggleShowPassword()">\n            <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n            <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n          </button>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n        <p>password required!</p>\n      </ion-item>\n    </form>\n    <div class="btnDiv">\n      <button ion-button class="btnLog" color="gpsc" (tap)="userlogin()">SIGN IN</button>\n      <ion-row>\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;" (tap)="forgotPassFunc()">Forgot Password?</ion-col>\n        <!-- <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">New user?&nbsp;&nbsp;\n          <span style="color:#14a4ce;" (tap)="gotosignuppage()">SIGNUP</span>\n        </ion-col> -->\n      </ion-row>\n    </div>\n    <!-- </div> -->\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]
            // public loadingCtrl: LoadingController
        ])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=29.js.map