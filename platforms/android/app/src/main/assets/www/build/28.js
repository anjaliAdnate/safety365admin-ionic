webpackJsonp([28],{

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(612);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */]),
            ],
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());

//# sourceMappingURL=settings.module.js.map

/***/ }),

/***/ 612:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsPage = /** @class */ (function () {
    function SettingsPage(events, tts, navCtrl, navParams, alertCtrl, toastCtrl) {
        this.events = events;
        this.tts = tts;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        if (localStorage.getItem("notifValue") != null) {
            this.notif = localStorage.getItem("notifValue");
            if (this.notif == 'true') {
                this.isChecked = true;
            }
            else {
                this.isChecked = false;
            }
        }
    }
    SettingsPage.prototype.ngOnInit = function () {
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") == 'live') {
                this.isBooleanforLive = true;
                this.isBooleanforDash = false;
            }
            else {
                if (localStorage.getItem("SCREEN") == 'dashboard') {
                    this.isBooleanforDash = true;
                    this.isBooleanforLive = false;
                }
            }
        }
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.setNotif = function (notif) {
        console.log(notif);
        this.events.publish('notif:updated', notif);
        this.isChecked = notif;
        if (notif === true) {
            this.tts.speak('voice notifications enabled ')
                .then(function () { return console.log('Success'); })
                .catch(function (reason) { return console.log(reason); });
        }
    };
    SettingsPage.prototype.rootpage = function () {
        // let alert = this.alertCtrl.create({
        //   title: 'Alert',
        //   subTitle: 'Choose default screen',
        var _this = this;
        // })
        var alert = this.alertCtrl.create();
        // alert.setTitle('Alert');
        alert.setSubTitle('Choose default screen');
        alert.addInput({
            type: 'radio',
            label: 'Dashboard',
            value: 'dashboard',
            checked: this.isBooleanforDash
        });
        alert.addInput({
            type: 'radio',
            label: 'Live Tracking',
            value: 'live',
            checked: this.isBooleanforLive
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log(data);
                localStorage.setItem("SCREEN", data);
                var toast = _this.toastCtrl.create({
                    message: 'Default page set to ' + data + ' page',
                    duration: 2000
                });
                toast.present();
            }
        });
        alert.present();
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/profile/settings/settings.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <ion-icon *ngIf="isChecked" name="mic" item-start></ion-icon>\n    <ion-icon *ngIf="!isChecked" name="mic-off" item-start></ion-icon>\n    <ion-label>Voice Notification</ion-label>\n    <ion-checkbox color="dark" [(ngModel)]="notif" (click)="setNotif(notif)" item-right></ion-checkbox>\n  </ion-item>\n  <ion-item (tap)="rootpage()">\n    <ion-icon name="checkmark-circle-outline" item-start></ion-icon>\n    <ion-label>Default Page</ion-label>\n  </ion-item>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/safety365admin-ionic/src/pages/profile/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ })

});
//# sourceMappingURL=28.js.map