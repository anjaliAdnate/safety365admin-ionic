import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";
import { LoadingController } from 'ionic-angular';

@Injectable()
export class ApiServiceProvider {
  token: any;

  private headers = new Headers();
  // private headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });

  _baseURL: string = "http://139.59.50.225/"
  usersURL: string = "http://139.59.50.225/users/";
  devicesURL: string = "http://139.59.50.225/devices";
  gpsURL: string = "http://139.59.50.225/gps";
  geofencingURL: string = "http://139.59.50.225/geofencing";
  trackRouteURL: string = "http://139.59.50.225/trackRoute";
  groupURL: string = "http://139.59.50.225/groups/";
  notifsURL: string = "http://139.59.50.225/notifs";
  stoppageURL: string = "http://139.59.50.225/stoppage";
  summaryURL: string = "http://139.59.50.225/summary";
  shareURL: string = "http://139.59.50.225/share";

  // http://www.vehtraschools.com/trackRoute/5c80b8efe589b2065f62baf4

  loading: any;
  constructor(
    public http: Http,
    public loadingCtrl: LoadingController) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    if (this.token == undefined) {
      if (localStorage.getItem("AuthToken") != null) {
        this.token = localStorage.getItem("AuthToken");
        console.log("Headers token => ", this.token)
        this.headers.append('Authorization', this.token);
      }
    }
  }

  ////////////////// LOADING SERVICE /////////////////

  startLoadingnew(key) {
    var str;
    if (key == 1) {
      str = 'unlocking';
    } else {
      str = 'locking';
    }
    return this.loading = this.loadingCtrl.create({
      content: "Please wait for some time, as we are " + str + " your vehicle...",
      spinner: "bubbles"
    });
  }

  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }
  ////////////////// END LOADING SERVICE /////////////

  getProfilestudentApi(payload) {
    if (this.token == undefined) {
      if (localStorage.getItem("AuthToken") != null) {
        this.token = localStorage.getItem("AuthToken");
        console.log("Headers token => ", this.token)
        this.headers.append('Authorization', this.token);
      }
    }
    console.log("headers: ", this.headers);
    return this.http.post("http://139.59.50.225/school/find", payload, { headers: this.headers })
      .map(res => res.json());
  }

  getTrackRoute(trackid, userid) {
    // http://www.vehtraschools.com/trackRoute/routepath/getRoutePathWithPoi?id=5c80b8efe589b2065f62baf4&user=5c4eeaa98522a1587337acff
    return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + trackid + '&user=' + userid, { headers: this.headers })
      .map(res => res.json());
  }

  callSearchService(email, id, key) {
    return this.http.get(this.devicesURL + "/getDeviceByUser?email=" + email + "&id=" + id + "&skip=0&limit=10&search=" + key, { headers: this.headers })
      .map(resp => resp.json());
  }

  callResponse(_id) {
    return this.http.get("http://139.59.50.225/trackRouteMap/getRideStatus?_id=" + _id, { headers: this.headers })
      .map(resp => resp.json());
  }

  serverLevelonoff(data) {
    return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  updatePassword(data) {
    return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  updateprofile(data) {
    return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
      .map(res => res.json());
  }

  getGeofenceCall(_id) {
    return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
      .map(res => res.json())
  }

  get7daysData(a, t) {
    return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
      .map(res => res.json());
  }

  dataRemoveFuncCall(_id, did) {
    return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
      .map(res => res.json())
  }

  tripReviewCall(device_id, stime, etime) {
    return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
      .map(res => res.json());
  }

  sendTokenCall(payLoad) {
    return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
      .map(res => res.json());
  }

  shareLivetrackCall(data) {
    return this.http.post(this.shareURL, data, { headers: this.headers })
      .map(res => res.json());
  }

  getDriverList(_id) {
    return this.http.get("http://139.59.50.225/driver/getDrivers?userid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  filterByDateCall(_id, skip: Number, limit: Number, dates) {
    // console.log("from date => "+ dates.fromDate.toISOString())
    // console.log("new date "+ new Date(dates.fromDate).toISOString())
    var from = new Date(dates.fromDate).toISOString();
    var to = new Date(dates.toDate).toISOString();
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  filterByType(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getFilteredcall(_id, skip: Number, limit: Number, key) {
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
      .map(res => res.json());
  }

  getDataOnScroll(_id, skip: Number, limit: Number) {
    return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleListCall(_id, email) {
    return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }

  trip_detailCall(_id, starttime, endtime, did) {
    return this.http.get('http://139.59.50.225/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteDataCall(data) {
    return this.http.post(this.trackRouteURL, data, { headers: this.headers })
      .map(res => res.json());
  }

  gettrackRouteCall(_id, data) {
    return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
      .map(res => res.json());
  }

  trackRouteCall(_id) {
    return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getRoutesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getStoppageApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getIgiApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getOverSpeedApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getGeogenceReportApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getFuelApi(starttime, endtime, Ignitiondevice_id, _id) {
    return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceReportApi(starttime, endtime, _id, Ignitiondevice_id) {
    return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
      .map(res => res.json());
  }

  getDailyReport(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());

  }

  contactusApi(contactdata) {
    return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
      .map(res => res.json());
  }

  getAllNotificationCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addgeofenceCall(data) {
    return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
      .map(res => res);
  }

  getdevicegeofenceCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofencestatusCall(_id, status, entering, exiting) {
    return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGeoCall(_id) {
    return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
      .map(res => res);
  }

  getallgeofenceCall(_id) {
    return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  user_statusCall(data) {
    return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
      .map(res => res);
  }

  editUserDetailsCall(devicedetails) {
    return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerVehiclesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addGroupCall(devicedetails) {
    return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getVehicleTypesCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getAllUsersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceModelCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  groupsCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  addDeviceCall(devicedetails) {
    return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }

  getCustomersCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  geofenceCall(_id) {
    return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassApi(mobno) {
    return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
      .map(res => res.json());
  }

  forgotPassMobApi(Passwordset) {
    return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
      .map(res => res.json());
  }

  loginApi(userdata) {
    return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
      .map(res => res.json());
  }

  signupApi(usersignupdata) {
    return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
      .map(res => res.json());
  }

  dashboardcall(email, from, to, _id) {
    return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  stoppedDevices(_id, email, off_ids) {
    return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
      .map(res => res.json());
  }

  livedatacall(_id, email) {
    return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
      .map(res => res.json());
  }

  getdevicesApi(_id, email) {
    return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
      .map(res => res.json());
  }
  getdevicesForAllVehiclesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  ignitionoffCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deviceupdateCall(devicedetail) {
    return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
      .map(res => res.json());
  }

  getDistanceSpeedCall(device_id, from, to) {
    return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  stoppage_detail(_id, from, to, device_id) {
    return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  gpsCall(device_id, from, to) {
    return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .map(res => res.json());
  }

  getcustToken(id) {
    return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
      .map(res => res.json());
  }

  getSummaryReportApi(starttime, endtime, _id, device_id) {
    return this.http.get(this.summaryURL + "?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
      .map(res => res.json());
  }

  getallrouteCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getSpeedReport(_id, time) {
    return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
      .map(res => res.json());
  }

  deleteDeviceCall(d_id) {
    return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }

  deviceShareCall(data) {
    return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
      .map(res => res.json());
  }

  pushnotifyCall(pushdata) {
    return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  pullnotifyCall(pushdata) {
    return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }


  getGroupCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  deleteGroupCall(d_id) {
    return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
      .map(res => res.json());
  }



  addcustomerCall(devicedetails) {
    return this.http.post(this.usersURL + 'signUp', devicedetails, { headers: this.headers })
      .map(res => res.json());
  }


  deleteCustomerCall(data) {
    return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
      .map(res => res.json());
  }

  getAllDealerCall(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  route_details(_id, user_id) {
    return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
      .map(res => res.json());
  }

  callcustomerSearchService(uid, pageno, limit, seachKey) {
    return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
      .map(res => res.json());
  }

}
