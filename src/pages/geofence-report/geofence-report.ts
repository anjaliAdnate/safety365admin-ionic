import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-geofence-report',
  templateUrl: 'geofence-report.html',
})
export class GeofenceReportPage implements OnInit {


  geofenceRepoert: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  geofencelist: any;
  devicesReport: any;
  geofencedata: any[];
  StartTime: string;
  Startetime: string;
  Startdate: string;
  datetime: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallGeofenceReport: ApiServiceProvider,
    public alertCtrl: AlertController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ngOnInit() {
    this.getgeofence1();
  }

  getgeofence1() {
    console.log("getgeofence shape");

    var baseURLp = 'http://139.59.50.225/geofencing/getallgeofence?uid=' + this.islogin._id;
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getallgeofenceCall(baseURLp)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.devices1243 = [];
        this.geofencelist = data;
        console.log("geofencelist=> ", this.geofencelist)

      },
        err => {
          this.apicallGeofenceReport.stopLoading();
          console.log(err)
        });
  }

  getGeofencedata(from, to, geofence) {
    console.log("selectedVehicle=> ", geofence)
    this.Ignitiondevice_id = geofence._id;
    this.getGeofenceReport(from, to);
  }


  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }


  getGeofenceReport(starttime, endtime) {
    console.log("this.Ignitiondevice_id=> "+ this.Ignitiondevice_id)
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    console.log(starttime);
    console.log(endtime);

    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getGeogenceReportApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();

        this.geofenceRepoert = data;
        console.log(this.geofenceRepoert);

        if (this.geofenceRepoert.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }
      
      }, error => {
        this.apicallGeofenceReport.stopLoading();
        console.log(error);

        // let alert = this.alertCtrl.create({
        //   message: "Please Select Geofencing",
        //   buttons: ['OK']
        // });
        // alert.present();

        // let alert = this.alertCtrl.create({
        //   message: 'No data found!',
        //   buttons: ['OK']
        // });
        // alert.present();
      });
  }

}
