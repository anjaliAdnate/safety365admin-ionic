import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-ignition-report',
  templateUrl: 'ignition-report.html',
})
export class IgnitionReportPage implements OnInit {

  islogin: any;
  Ignitiondevice_id: any;
  igiReport: any;

  datetimeEnd: any;
  datetimeStart: string;
  devices1243: any[];
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);

  }

  ngOnInit() {
    this.getdevices();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getdevices() {
    this.apicalligi.startLoading().present();
    this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getIgnitiondevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle.Device_Name;
    // this.getIgnitiondeviceReport(from, to);
  }

  getIgnitiondeviceReport(starttime, endtime) {

    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";

    }
    // console.log(starttime);
    // console.log(endtime);

    this.apicalligi.startLoading().present();

    this.apicalligi.getIgiApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.igiReport = data;
        console.log(this.igiReport);

        if (this.igiReport.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicalligi.stopLoading();
        this.apicalligi.stopLoading();
        // console.log(error);
        // let alert = this.alertCtrl.create({
        //   message: 'No data found!',
        //   buttons: ['OK']
        // });
        // alert.present();
      })
  }


}
