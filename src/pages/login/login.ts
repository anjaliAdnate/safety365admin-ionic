import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { SignupPage } from '../signup/signup';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { DashboardPage } from '../dashboard/dashboard';
// import { MobileVerify } from './mobile-verify';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  submitAttempt: boolean;
  otpMess: any;
  responseMessage: string;
  data: {};
  logindata: any;
  userDetails: string;
  details: any;
  showPassword: boolean = false;
  // loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public apiservice: ApiServiceProvider,
    public toastCtrl: ToastController,
    public events: Events
    // public loadingCtrl: LoadingController
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  userlogin() {
    this.submitAttempt = true;

    if (this.loginForm.value.username == "") {
      /*alert("invalid");*/
      return false;
    }
    else if (this.loginForm.value.password == "") {
      /*alert("invalid");*/
      return false;
    }

    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(this.loginForm.value.username);
    var isName = isNaN(this.loginForm.value.username);

    if (isEmail == false && isName == false) {
      this.data = {

        "psd": this.loginForm.value.password,
        "ph_num": this.loginForm.value.username
      };
    }
    else if (isEmail) {

      this.data = {

        "psd": this.loginForm.value.password,
        "emailid": this.loginForm.value.username
      };
    }
    else {

      this.data = {

        "psd": this.loginForm.value.password,
        "user_id": this.loginForm.value.username
      };
    }

    this.apiservice.startLoading();
    this.apiservice.loginApi(this.data)
      .subscribe(response => {
        this.logindata = response;

        this.events.publish("auth:token", response.token);
        localStorage.setItem("AuthToken", response.token);

        this.logindata = JSON.stringify(response);
        var logindetails = JSON.parse(this.logindata);
        this.userDetails = window.atob(logindetails.token.split('.')[1]);
        this.details = JSON.parse(this.userDetails);
        console.log(this.details.email);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(this.details));
        localStorage.setItem('condition_chk', this.details.isDealer);

        this.apiservice.stopLoading();

        let toast = this.toastCtrl.create({
          message: "Welcome! You're logged In successfully.",
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.navCtrl.setRoot('DashboardPage');
        });

        toast.present();

        this.responseMessage = "LoggedIn  successfully";
      },
        error => {
          // console.log("login error => "+error)
          var body = error._body;
          var msg = JSON.parse(body);
          if (msg.message == "Mobile Phone Not Verified") {
            let confirmPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
              buttons: [
                { text: 'Cancel' },
                {
                  text: 'Ok',
                  handler: () => {
                    // this.navCtrl.push(MobileVerify);
                  }
                }
              ]
            });
            confirmPopup.present();
          } else {
            // Do something on error
            let alertPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message,
              buttons: ['OK']
            });
            alertPopup.present();
          }
          this.responseMessage = "Something Wrong";
          this.apiservice.stopLoading();
        });
  }

  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

  forgotPassFunc() {

    const prompt = this.alertCtrl.create({
      title: 'Forgot Password',
      message: "Enter your Registered  Mobile Number we will send you a confirmation code",
      inputs: [
        {
          name: 'mobno',
          placeholder: 'Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'SEND CONFIRMATION CODE',
          handler: data => {
            // console.log('Saved clicked');
            // console.log(data.mobno);
            var forgotdata = {
              "cred": data.mobno
            }
            this.apiservice.startLoading();
            this.apiservice.forgotPassApi(forgotdata)
              .subscribe(data => {
                this.apiservice.stopLoading();
                this.presentToast(data.message);
                this.otpMess = data;
                console.log(this.otpMess);

                this.PassWordConfirmPopup();
                this.responseMessage = "sent code successfully";
              },
                error => {
                  this.apiservice.stopLoading();
                  var body = error._body;
                  var msg = JSON.parse(body);
                  let alert = this.alertCtrl.create({
                    title: "Forgot Password Failed!",
                    message: msg.message,
                    buttons: ['OK']
                  });
                  alert.present();
                })
          }
        }
      ]
    });
    prompt.present();
  }

  PassWordConfirmPopup() {
    const prompt = this.alertCtrl.create({
      title: 'Reset Password',
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'newpass',
          placeholder: 'Password'
        },
      ],
      buttons: [
        {
          text: 'Back',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'SAVE',
          handler: data => {
            console.log('Saved clicked');
            if (!data.newpass || !data.mobilenum || !data.confCode) {
              let alertPopup = this.alertCtrl.create({
                title: 'Warning!',
                message: "Fill all mandatory fields!",
                buttons: ['OK']
              });
              alertPopup.present();
            } else {
              if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                if (data.newpass.length < 6 || data.newpass.length > 12) {
                  var Popup = this.alertCtrl.create({
                    title: 'Warning!',
                    message: "Password length should be 6 - 12",
                    buttons: ['OK']
                  });
                  Popup.present();
                } else {
                  var Passwordset = {
                    "newpwd": data.newpass,
                    "otp": data.confCode,
                    "phone": this.otpMess,
                    "cred": this.otpMess
                  }
                  this.apiservice.startLoading();
                  this.apiservice.forgotPassMobApi(Passwordset)
                    .subscribe(data => {
                      this.apiservice.stopLoading();
                      this.presentToast(data.message);
                      this.navCtrl.setRoot(LoginPage);
                      this.responseMessage = "password changed successfully";
                    },
                      error => {
                        this.apiservice.stopLoading();
                        var body = error._body;
                        var msg = JSON.parse(body);
                        let alert = this.alertCtrl.create({
                          title: "Forgot Password failed!",
                          message: msg.message,
                          buttons: ['OK']
                        });
                        alert.present();
                        this.responseMessage = "Something Wrong";
                      });
                }
              } else {
                let alertPopup = this.alertCtrl.create({
                  title: 'Warning!',
                  message: "New Password and Confirm Password Not Matched",
                  buttons: ['OK']
                });
                alertPopup.present();
              }
              if (!data.newpass || !data.mobilenum || !data.confCode) {
                //don't allow the user to close unless he enters model...
                return false;
              }
            }
          }
        }
      ]
    });
    prompt.present();
  }
}
