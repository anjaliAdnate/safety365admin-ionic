import { NavParams, IonicPage } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { GoogleMaps, Marker, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon } from '@ionic-native/google-maps';

@IonicPage()
@Component({
    templateUrl: './geofence-show.html',
    styles: [`
    #mapShowGeofence{
        height: 100%;
        width: 100%;
     }`]
})
export class GeofenceShowPage implements OnInit {
    items: any;
    gefence: any;
    gefencename: any;
    geofenceShowdata: any;
    geoshape: any = {};
    GeoFencCoords: any;
    alldata: any = {};
    mapdata: any[];

    constructor(navParam: NavParams, public apiCall: ApiServiceProvider) {
        this.items = navParam.get("param");
        this.gefence = this.items._id;
        this.gefencename = this.items.geoname;
        console.log("gefencename=> " + this.gefencename);
    }

    ngOnInit() {
        this.getgeofence();
    }

    newMap() {
        let mapOptions = {
            camera: {
                zoom: 15
            },
            controls: {
                zoom: true
            },
            mapTypeControlOptions: {
                mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            }
        }
        let map = GoogleMaps.create('mapShowGeofence', mapOptions);
        return map;
    }

    getgeofence() {
        var baseURLp = 'http://139.59.50.225/geofencing/getdevicegeofence?gid=' + this.gefence;
        this.apiCall.startLoading().present();
        this.apiCall.getdevicegeofenceCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.geofenceShowdata = data;
                this.geofenceShowdata = data[0].geofence.coordinates[0];
                console.log(this.geofenceShowdata);

                this.geoshape = [];

                this.geoshape = this.geofenceShowdata.map(function (p) {
                    return {
                        lat: p[1], lng: p[0],
                    };
                });
                var temp = JSON.stringify(this.geoshape);
                var temp2 = JSON.parse(temp);
                console.log(temp2)

                // this.draw(temp2);
                this.drawPolygon(temp2);
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err)
                });
    }

    drawPolygon(polyData) {
        let that = this;
        that.alldata.map = that.newMap();
        that.mapdata = [];
        that.mapdata = polyData.map(function (d) {
            return { lat: d.lat, lng: d.lng }
        });

        let bounds = new LatLngBounds(that.mapdata);
        that.alldata.map.moveCamera({
            target: bounds
        });
        let GORYOKAKU_POINTS: ILatLng[] = that.mapdata;

        that.alldata.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 3
        }).then((polygon: Polygon) => {
           
            polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
                console.log("polygon data=> " + param)
                console.log("polygon data=> " + param[1])
                var contentString = '<b>Geo Fence</b><br>' +
                    'Name: ' + that.gefencename;
                    let marker: Marker = param[1];
                    marker.showInfoWindow();
                    alert(contentString);
            })
        });
    }
}