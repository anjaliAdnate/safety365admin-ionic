import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { IonPullUpFooterState } from 'ionic-pullup';
import { AppVersion } from '@ionic-native/app-version';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { TextToSpeech } from '@ionic-native/text-to-speech';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  islogin: any;
  token: string;
  aVer: any;
  isDealer: boolean;
  footerState: IonPullUpFooterState;
  credentialsForm: FormGroup;
  constructor(
    private appVersion: AppVersion,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private formBuilder: FormBuilder,
    public plt: Platform
    // private tts: TextToSpeech
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.appVersion.getVersionNumber().then((version) => {
      this.aVer = version;
      console.log("app version=> " + this.aVer);
    });
    this.isDealer = this.islogin.isDealer;
    console.log("isDealer=> " + this.isDealer)

    this.footerState = IonPullUpFooterState.Collapsed;

    this.credentialsForm = this.formBuilder.group({
      fname: [this.islogin.fn],
      lname: [this.islogin.ln],
      email: [this.islogin.email],
      phonenum: [this.islogin.phn],
      org: [this.islogin._orgName],
      // dealer: []
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }


  settings() {
    this.navCtrl.push('SettingsPage');
  }

  service() {
    this.navCtrl.push(ServiceProviderPage, {
      param: this.islogin
    })
  }

  password() {
    this.navCtrl.push(UpdatePasswordPage, {
      param: this.islogin
    })
  }

  onSignIn() {
    // this.logger.info('SignInPage: onSignIn()');
    console.log(this.credentialsForm.value)
    var data = {
      "fname": this.credentialsForm.value.fname,
      "lname": this.credentialsForm.value.lname,
      "org": this.credentialsForm.value.org,
      "noti": true,
      "uid": this.islogin._id
    }
    this.apiCall.startLoading().present();
    this.apiCall.updateprofile(data)
      .subscribe(resdata => {
        this.apiCall.stopLoading();
        console.log("response from server=> ", resdata);
        if (resdata.token) {
          let alert = this.alertCtrl.create({
            message: 'Profile updated succesfully!',
            buttons: [{
              text: 'OK',
              handler: () => {

                var logindata = JSON.stringify(resdata);
                var logindetails = JSON.parse(logindata);
                var userDetails = window.atob(logindetails.token.split('.')[1]);
                var details = JSON.parse(userDetails);
                console.log(details.email);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(details));
                localStorage.setItem('condition_chk', details.isDealer);

                this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                this.footerState = 1;
                this.toggleFooter();
              }
            }]
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error from service=> ", err)
        });
  }

  logout() {
    this.token = localStorage.getItem("DEVICE_TOKEN");
    var ostype;
    if(this.plt.is('ios')) {
      ostype = "ios";
    } else {
      if(this.plt.is('android')) {
        ostype = "android";
      }
    }
    var pushdata = {
      "uid": this.islogin._id,
      "token": this.token,
      "os": ostype
    }

    let alert = this.alertCtrl.create({
      message: 'Do you want to logout from the application?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.pullnotifyCall(pushdata)
            .subscribe(data => {
              this.apiCall.stopLoading();
              console.log("push notifications updated " + data.message)
              localStorage.clear();
              localStorage.setItem('count', null)
              // this.menuCtrl.close();
              this.navCtrl.setRoot('LoginPage');

            },
              err => {
                this.apiCall.stopLoading();
                console.log(err)
              });
        }
      },
      {
        text: 'No',
        handler: () => {
          // this.menuCtrl.close();
        }
      }]
    });
    alert.present();
  }

  // setNotif(notif) {
  //   console.log(notif);
  //   this.events.publish('notif:updated', notif);
  //   this.isChecked = notif;
  //   if (notif === true) {
  //     this.tts.speak('You have succesfully enabled voice notifications')
  //       .then(() => console.log('Success'))
  //       .catch((reason: any) => console.log(reason));
  //   }
  // }
}

@Component({
  templateUrl: './service-provider.html',
  selector: 'page-profile'
})

export class ServiceProviderPage {
  uData: any = {};
  sorted: any = [];
  constructor(public navParam: NavParams) {
    this.uData = this.navParam.get("param");
    if(this.uData.Dealer_ID != undefined) {
      this.sorted = this.uData.Dealer_ID;
    } else {
      this.sorted.first_name = this.uData.fn;
      this.sorted.last_name = this.uData.fn;
      this.sorted.phone = this.uData.phn;
      this.sorted.email = this.uData.email;
    }
    console.log("udata=> ", this.uData)
    console.log("udata=> ", JSON.stringify(this.uData))
  }
}

@Component({
  selector: 'page-profile',
  templateUrl: './update-password.html'
})

export class UpdatePasswordPage {
  passData: any;
  cnewP: any;
  newP: any;
  oldP: any;
  constructor(
    public navParam: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiSrv: ApiServiceProvider,
    public navCtrl: NavController
  ) {
    this.passData = this.navParam.get("param");
    console.log("passData=> " + JSON.stringify(this.passData))
  }

  savePass() {
    if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
      let alert = this.alertCtrl.create({
        message: 'Fields should not be empty!',
        buttons: ['OK']
      });
      alert.present();
    } else {
      if (this.newP != this.cnewP) {
        let alert = this.alertCtrl.create({
          message: 'Password Missmatched!!',
          buttons: ['Try Again']
        });
        alert.present();
      } else {
        var data = {
          "ID": this.passData._id,
          "OLD_PASS": this.oldP,
          "NEW_PASS": this.newP
        }
        this.apiSrv.startLoading().present();
        this.apiSrv.updatePassword(data)
          .subscribe(respData => {
            this.apiSrv.stopLoading();
            console.log("respData=> ", respData)
            const toast = this.toastCtrl.create({
              message: 'Password Updated successfully',
              position: "bottom",
              duration: 2000
            });
            toast.onDidDismiss(() => {
              this.oldP = "";
              this.newP = "";
              this.cnewP = "";
            });
            toast.present();
          },
            err => {
              this.apiSrv.stopLoading();
              console.log("error in update password=> ", err)
              // debugger
              if (err.message == "Timeout has occurred") {
                // alert("the server is taking much time to respond. Please try in some time.")
                let alerttemp = this.alertCtrl.create({
                  message: "the server is taking much time to respond. Please try in some time.",
                  buttons: [{
                    text: 'Okay',
                    handler: () => {
                      this.navCtrl.setRoot("DashboardPage");
                    }
                  }]
                });
                alerttemp.present();
              } else {
                const toast = this.toastCtrl.create({
                  message: err._body.message,
                  position: "bottom",
                  duration: 2000
                });
                toast.onDidDismiss(() => {
                  this.oldP = "";
                  this.newP = "";
                  this.cnewP = "";
                  this.navCtrl.setRoot("DashboardPage");
                });
                toast.present();
              }
            });
      }
    }

  }
}
