import { Component, OnInit } from "@angular/core";
import { NavParams, ViewController, ToastController, AlertController, ModalController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
// import { GroupModalPage } from "./group-modal/group-modal";
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-add-devices',
    templateUrl: 'add-device-modal.html'
})

export class AddDeviceModalPage implements OnInit {
    addvehicleForm: FormGroup;
    submitAttempt: boolean;
    customer: any;
    devicesadd: any;
    islogin: any;
    allGroup: any;
    allGroupName: any;
    deviceModel: any;
    selectUser: any;
    allVehicle: any;
    modeldata: any;
    groupstaus: any;
    userdata: any;
    vehType: any;
    groupstaus_id: any;
    devicedetails: any = {};
    TypeOf_Device: any;
    driverList: any;

    minDate: any;
    currentYear: any;
    modeldata_id: any;
    constructor(
        params: NavParams,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        public apiCall: ApiServiceProvider,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        public modalCtrl: ModalController
    ) {

        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);

        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> "+ moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"))
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', Validators.required],
            device_id: ['', Validators.required],
            driver: [''],
            contact_num: ['', Validators],
            sim_number: ['', Validators.required],
            ExipreDate: [this.currentYear, Validators.required],
            device_type: ['', Validators.required],
            name: [''],
            first_name: [''],
            brand: ['', Validators.required]
        });

        if (params.get("custDet")) {
            this.islogin = params.get("custDet");
            console.log("custDel=> ", this.islogin);
        } else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + this.islogin);
        }

    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    ngOnInit() {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    }

    getDrivers() {
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(data => {
                this.driverList = data;
                console.log("driver list => " + this.driverList)
            },
                err => {
                    console.log(err);
                });
    }


    openAddGroupModal() {
        let modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(() => {
            console.log("modal dismissed!")
        })
        modal.present();
    }

    adddevices() {
        // console.log(devicedetails);
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.groupstaus == undefined) {
                this.groupstaus_id = this.islogin._id;

            } else {
                this.groupstaus_id = this.groupstaus._id;
            }

            this.devicedetails = {
                "devicename": this.addvehicleForm.value.device_name,
                "deviceid": this.addvehicleForm.value.device_id,
                "driver_name": this.addvehicleForm.value.driver,
                "contact_number": this.addvehicleForm.value.contact_num,
                "typdev": "Tracker",
                "sim_number": this.addvehicleForm.value.sim_number,
                "user": this.islogin._id,
                "emailid": this.islogin.email,
                "iconType": this.TypeOf_Device,
                "vehicleGroup": this.groupstaus_id,
                "device_model": this.modeldata_id,
                "expiration_date": new Date(this.addvehicleForm.value.ExipreDate).toISOString()

            }

            if (this.vehType == undefined) {
                this.devicedetails;
            } else {
                this.devicedetails.vehicleType = this.vehType._id;
            }

            console.log("devicedetails=> " + this.devicedetails);

            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.devicesadd = data;
                    console.log("devicesadd=> ", this.devicesadd)
                    let toast = this.toastCtrl.create({
                        message: 'Vehicle was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss(this.vehType);
                    });

                    toast.present();
                },
                    err => {
                        this.apiCall.stopLoading();
                        var body = err._body;
                        var msg = JSON.parse(body);
                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: msg.errmsg,
                            buttons: ['Try Again']
                        });
                        alert.present();
                        console.log("err");
                    });
        }
    }

    getGroup() {
        console.log("get group");
        var baseURLp = 'http://139.59.50.225/groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.allGroup = data;
                this.allGroup = data["group details"]
                // console.log("all group=> " + this.allGroup[0].name);
                for (var i = 0; i < this.allGroup.length; i++) {
                    this.allGroupName = this.allGroup[i].name;
                    // console.log("allGroupName=> "+this.allGroupName);
                }
                console.log("allGroupName=> " + this.allGroupName)
            },
                err => {
                    console.log(err);
                    this.apiCall.stopLoading();
                });
    }

    getDeviceModel() {
        console.log("getdevices");
        var baseURLp = 'http://139.59.50.225/deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(data => {
                this.deviceModel = data;
            },
                err => {
                    console.log(err);
                });
    }

    getSelectUser() {
        console.log("get user");

        var baseURLp = 'http://139.59.50.225/users/getAllUsers?dealer=' + this.islogin._id;

        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(data => {
                this.selectUser = data;
            },
                error => {
                    console.log(error)
                });
    }

    getVehicleType() {
        console.log("get getVehicleType");

        var baseURLp = 'http://139.59.50.225/vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(data => {
                // this.apiCall.stopLoading();
                this.allVehicle = data;
            }, err => {
                console.log(err);
                // this.apiCall.stopLoading();
            });

    }

    deviceModelata(deviceModel) {
        console.log("deviceModel" + deviceModel);

        if (deviceModel != undefined) {
            this.modeldata = deviceModel;
            this.modeldata_id = this.modeldata._id;
        } else {
            this.modeldata_id = null;
        }
        // console.log("modal data device_type=> " + this.modeldata.device_type);
    }

    GroupStatusdata(status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    }

    userselectData(userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    }

    vehicleTypeselectData(vehicletype) {
        console.log(vehicletype);
        this.vehType = vehicletype;
        console.log("vehType=> " + this.vehType._id);
    }
}