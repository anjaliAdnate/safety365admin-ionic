import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

// import { EditRouteDetailsPage } from './edit-route-details/edit-route-details';
// import { RouteMapShowPage } from '../route-map-show/route-map-show';
// import { CreateRoutePage } from './create-route/create-route';

@IonicPage()
@Component({
  selector: 'page-route',
  templateUrl: 'route.html',
})
export class RoutePage implements OnInit {
  islogin: any;
  routesdata: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController, ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log(this.islogin._id);
  }

  ngOnInit() {
    this.getRoutes();
  }

  getRoutes() {
    console.log("getRoutes");

    var baseURLp = 'http://139.59.50.225/trackRoute/user/' + this.islogin._id;
    //console.log(baseURLp);
    this.apiCall.startLoading().present();
    this.apiCall.getRoutesCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.routesdata = data.reverse();
        console.log(this.routesdata);
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  // createRoute() {
  //   this.navCtrl.push(CreateRoutePage)
  // }

  // searchUser(ev: any) {
  //   // Reset items back to all of the items
  //   // this.getcustomer();

  //   // set val to the value of the searchbar
  //   let val = ev.target.value;

  //   // if the value is an empty string don't filter the items
  //   if (val && val.trim() != '') {
  //     this.routesdata = this.routesdata.filter((item) => {
  //       return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
  //     })
  //   }
  // }

  ShowRoute(routes) {
    console.log('data pass route', routes)
    this.navCtrl.push('RouteMapShowPage', {
      param: routes
    });
  }

  // openroute_edit(routes) {
  //   console.log('Opening Modal open update deviceModal');
  //   let modal = this.modalCtrl.create(EditRouteDetailsPage, {
  //     param: routes
  //   })
  //   modal.present();
  //   // $rootScope.routesdetail = angular.copy(routes1);
  //   // console.log($rootScope.routesdetail);
  // };

  deleteFunc(_id) {
    // var link = 'http://139.59.50.225/trackRoute/' + _id;
    this.apiCall.startLoading().present();
    this.apiCall.trackRouteCall(_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        if (data) {
          // console.log(this.DeletedDevice);
          let toast = this.toastCtrl.create({
            message: 'Route was deleted successfully',
            position: 'bottom',
            duration: 1500
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            this.getRoutes();
          });

          toast.present();
        }

      },
        err => {
          this.apiCall.stopLoading();
          console.log("deleteing route error=> " + err)
          let toast = this.toastCtrl.create({
            message: 'Route was deleted successfully',
            position: 'bottom',
            duration: 1500
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
            this.getRoutes();
          });

          toast.present();
          // var body = err._body;
          // var msg = JSON.parse(body);
          // let alert = this.alertCtrl.create({
          //   title: 'Oops!',
          //   message: msg.message,
          //   buttons: ['OK']
          // });
          // alert.present();
        });
  }

  DelateRoute(data) {
    console.log(data._id);
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this route?',
      buttons: [{
        text: 'NO'
      },
      {
        text: 'YES',
        handler: () => {
          this.deleteFunc(data._id);
        }
      }]
    });
    alert.present();
  }

}
