import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { DatePicker } from '@ionic-native/date-picker';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})
export class DailyReportPage implements OnInit {

  deviceReport: any;
  deviceReportSearch: any = [];

  to: string;
  from: string;
  islogin: any;
  todaydate: Date;
  todaytime: string;
  datetime: number;

  page: number = 0;
  limit: number = 6;
  fromDate: Date;
  vehicleData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalldaily: ApiServiceProvider,
    // private datePicker: DatePicker
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.from = moment({ hours: 0 }).format();
    console.log('start date', this.from)
    this.to = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.to);

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }

  }

  ngOnInit() {
    this.getDailyReportData();
  }

  getItems(ev: any) {
    // console.log(ev.target.value, this.deviceReport)
    const val = ev.target.value.trim();
    this.deviceReportSearch = this.deviceReport.filter((item) => {
      return (item.device.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    // console.log("search====", this.deviceReportSearch);
  }

  // showDateTime() {
  //   let that = this;
  //   this.datePicker.show({
  //     date: new Date(),
  //     mode: 'datetime',
  //     androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
  //   }).then(
  //     date => {
  //       console.log('Got date: ', date);
  //       that.fromDate = date;
  //     },
  //     err => console.log('Error occurred while getting date: ', err)
  //   );
  // }


  getDailyReportData() {
    this.page = 0;
    var baseUrl;
    if (this.vehicleData == undefined) {
      baseUrl = "http://139.59.50.225/gps/getGpsReport?email=" + this.islogin.email + '&id=' + this.islogin._id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString() + '&s=' + this.page + '&l=' + this.limit;
    } else {
      baseUrl = "http://139.59.50.225/gps/getGpsReport?email=" + this.islogin.email + '&id=' + this.islogin._id + '&from=' + new Date(this.from).toISOString() + '&to=' + new Date(this.to).toISOString() + '&s=' + this.page + '&l=' + this.limit + '&devId=' + this.vehicleData.Device_ID;
    }

    this.apicalldaily.startLoading().present();
    this.apicalldaily.getDailyReport(baseUrl)
      .subscribe(data => {
        this.apicalldaily.stopLoading();
        this.deviceReport = data.data;
        this.deviceReportSearch = data.data;

      }, error => {
        this.apicalldaily.stopLoading();
        console.log("error in service=> " + error);
      })
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    var tempNum = 0;
    tempNum = (that.page * that.limit);
    setTimeout(() => {
      var baseUrl;
      if (that.vehicleData == undefined) {
        baseUrl = "http://139.59.50.225/gps/getGpsReport?email=" + that.islogin.email + '&id=' + that.islogin._id + '&from=' + new Date(that.from).toISOString() + '&to=' + new Date(that.to).toISOString() + '&s=' + tempNum + '&l=' + that.limit;
      } else {
        baseUrl = "http://139.59.50.225/gps/getGpsReport?email=" + that.islogin.email + '&id=' + that.islogin._id + '&from=' + new Date(that.from).toISOString() + '&to=' + new Date(that.to).toISOString() + '&s=' + tempNum + '&l=' + that.limit + '&devId=' + that.vehicleData.Device_ID;
      }
      that.apicalldaily.getDailyReport(baseUrl)
        .subscribe(
          res => {
            for (let i = 0; i < res.data.length; i++) {
              that.deviceReport.push(res.data[i]);
            }
            that.deviceReportSearch = that.deviceReport;
          },
          error => {
            console.log(error);
          });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 200);
  }
}
