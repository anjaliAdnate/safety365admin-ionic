import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
declare var google;
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-device-summary-repo',
  templateUrl: 'device-summary-repo.html',
})
export class DeviceSummaryRepoPage implements OnInit {

  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  device_id: any;
  summaryReport: any;
  locationEndAddress: any;
  locationAddress: any;
  datetime: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apicallsummary: ApiServiceProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
   
  }


  ngOnInit() {
    this.getdevices();
  }


  change(datetimeStart) {
    console.log("datetimeStart=> "+ datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }


  getSummaarydevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.device_id = selectedVehicle._id;
    // this.getIgnitiondeviceReport(from, to);
  }

  getdevices() {
    this.apicallsummary.startLoading().present();
    this.apicallsummary.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicallsummary.stopLoading();
          console.log(err);
        });
  }


  summaryReportData = [];
  getSummaryReport(starttime, endtime) {
    this.summaryReportData = [];
    var outerthis = this;
    if (this.device_id == undefined) {
      this.device_id = "";

    }
    console.log(starttime);
    console.log(endtime);

    this.apicallsummary.startLoading().present();
    this.apicallsummary.getSummaryReportApi(new Date(starttime).toISOString(), new Date(endtime).toISOString(), this.islogin._id, this.device_id)
      .subscribe(data => {
        this.apicallsummary.stopLoading();
        this.summaryReport = data;
        console.log(this.summaryReport);

        var i = 0, howManyTimes = this.summaryReport.length;
        function f() {
          outerthis.summaryReportData.push({ 'avgSpeed': outerthis.summaryReport[i].avgSpeed, 'Device_Name': outerthis.summaryReport[i].device.Device_Name, 'routeViolations': outerthis.summaryReport[i].routeViolations, 'overspeeds': outerthis.summaryReport[i].overspeeds, 'ignOn': outerthis.summaryReport[i].ignOn, 'ignOff': outerthis.summaryReport[i].ignOff, 'distance': outerthis.summaryReport[i].distance, 'tripCount': outerthis.summaryReport[i].tripCount });
          // outerthis.summaryReportData.push({ 'distance': outerthis.summaryReport[i].distance, 'Device_Name': outerthis.summaryReport[i].device.Device_Name });
          if (outerthis.summaryReport[i].endPoint != null && outerthis.summaryReport[i].startPoint != null) {
            var latEnd = outerthis.summaryReport[i].endPoint[0];
            var lngEnd = outerthis.summaryReport[i].endPoint[1];
            var latlng = new google.maps.LatLng(latEnd, lngEnd);

            var latStart = outerthis.summaryReport[i].startPoint[0];
            var lngStart = outerthis.summaryReport[i].startPoint[1];


            var lngStart1 = new google.maps.LatLng(latStart, lngStart);

            var geocoder = new google.maps.Geocoder();

            var request = {
              latLng: latlng
            };

            var request1 = {
              latLng: lngStart1
            };


            geocoder.geocode(request, function (data, status) {
              console.log(data);
              if (status == google.maps.GeocoderStatus.OK) {
                if (data[1] != null) {
                  console.log("End address is: " + data[1].formatted_address);
                  outerthis.locationEndAddress = data[1].formatted_address;

                } else {
                  console.log("No address available")
                }
              }
              else {
                console.log(" address available")
              }
              outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = outerthis.locationEndAddress;
            })

            geocoder.geocode(request1, function (data, status) {
              console.log(data);
              if (status == google.maps.GeocoderStatus.OK) {
                if (data[1] != null) {
                  console.log("Start address is: " + data[1].formatted_address);
                  outerthis.locationAddress = data[1].formatted_address;

                } else {
                  console.log("No address available")
                }
              }
              else {
                console.log(" address available")
              }
              outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = outerthis.locationAddress;
            })

          }
          console.log(outerthis.summaryReportData);

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 5000);
          }

        }
        f();

      }, error => {
        this.apicallsummary.stopLoading();
        console.log(error);
      })
  }


}
