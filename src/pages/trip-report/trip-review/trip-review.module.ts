import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TripReviewPage } from './trip-review';

@NgModule({
  declarations: [
    TripReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(TripReviewPage),
    SelectSearchableModule
  ],
})
export class TripReviewPageModule {}
