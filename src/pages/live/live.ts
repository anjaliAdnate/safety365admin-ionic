import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import * as moment from 'moment';
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon } from '@ionic-native/google-maps';
declare var google;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage implements OnInit, OnDestroy {

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  mapData1: any[];
  routeData: any;
  trackRouteID: any;
  deviceDeatils: any;
  centerPonitLat: any;
  centerPointLng: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public plt: Platform
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  busIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = { "bus": this.busIcon }

  ngOnInit() {
    this.onClickShow = false;
    this.getProfilestudent();
    this._io = io.connect('http://139.59.50.225/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;
    this.userDevices();
  }

  ngOnDestroy() {
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }

  newMap() {
    let mapOptions = {
      controls: {
        zoom: false
      },
      gestures: {
        rotate: false,
        tilt: false
      }
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    map.animateCamera({
      target: { lat: 20.5937, lng: 78.9629 },
      duration: 2000,
      padding: 0,
    })
    return map;
  }

  userDevices() {
    let that = this;
    if (that.allData.map != undefined) {
      that.allData.map.remove();
      that.allData.map = that.newMap();
    } else {
      that.allData.map = that.newMap();
    }

    that.apiCall.startLoading().present();
    that.apiCall.getdevicesApi(that.userdetails._id, that.userdetails.email)
      .subscribe(resp => {
        that.apiCall.stopLoading();
        that.portstemp = resp.devices;
        that.mapData = [];
        that.mapData = that.portstemp.map(function (d) {
          if (d.last_location != undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          }
        });

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })
        // for (var i = 0; i < resp.devices.length; i++) {
        //   that.socketInit(resp.devices[i]);
        // }
        // debugger;
        for (let d in resp.devices) {
          that.allData.marker = [];
          that.socketInit(resp.devices[d]);
        }
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err);
        });
  }

  socketInit(pdata) {
    // debugger
    let that = this;
    let key = pdata._id;

    if (that.allData.marker[key] == undefined) {
      if (pdata && pdata.last_location && pdata.last_location.lat && pdata.last_location.long) {
        let ddd: any = { lat: pdata.last_location.lat, lng: pdata.last_location.long }
        let icon = "";
        if (moment().diff(moment(pdata.last_ping_on), 'minutes') < 60) {
          if (that.plt.is('ios')) {
            icon = 'www/assets/imgs/vehicles/runningbus.png';
          } else {
            if (that.plt.is('android')) {
              icon = './assets/imgs/vehicles/runningbus.png';
            } else {
              icon = './assets/imgs/vehicles/runningbus.png';
            }
          }
        }
        else {
          if (that.plt.is('ios')) {
            icon = 'www/assets/imgs/vehicles/stoppedbus.png';
          } else {
            if (that.plt.is('android')) {
              icon = './assets/imgs/vehicles/stoppedbus.png';
            } else {
              icon = './assets/imgs/vehicles/stoppedbus.png';
            }
          }
        }
        that.allData.map.addMarker({
          position: ddd,
          title: pdata.Device_Name,
          icon: icon
        }).then((marker: Marker) => {
          that.allData.marker[pdata._id] = marker;
          that.allData.marker[pdata._id].dicon = icon;
        })
      }
    }
    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 == null) return;

      let dd1: any = { lat: d4.last_location.lat, lng: d4.last_location.long };
      let dd2: any = { lat: d4.sec_last_location.lat, lng: d4.sec_last_location.long };
      // debugger
      if (that.allData.marker[d4._id] == undefined) {

        // if(that.allData.marker[key])
        that.allData.marker[key].remove();
        
        var icon1;
        if (that.plt.is('ios')) {
          icon1 = 'www/assets/imgs/vehicles/runningbus.png';
        } else {
          if (that.plt.is('android')) {
            icon1 = './assets/imgs/vehicles/runningbus.png';
          } else {
            icon1 = './assets/imgs/vehicles/runningbus.png';
          }
        }

        that.allData.map.addMarker({
          position: dd1,
          title: d4.Device_Name,
          icon: icon1
        }).then((marker: Marker) => {
          debugger
          that.allData.marker[d4._id] = marker;
          that.allData.marker[d4._id].dicon = icon1;
        })
      } else {
        let icon = "";
        if (moment().diff(moment(d4.last_ping_on), 'minutes') < 60) {
          if (that.plt.is('ios')) {
            that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/runningbus.png';
          } else {
            if (that.plt.is('android')) {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/runningbus.png';
            } else {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/runningbus.png';
            }
          }
        } else {
          if (that.plt.is('ios')) {
            that.allData.marker[d4._id].icon = 'www/assets/imgs/vehicles/stoppedbus.png';
          } else {
            if (that.plt.is('android')) {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/stoppedbus.png';
            } else {
              that.allData.marker[d4._id].icon = './assets/imgs/vehicles/stoppedbus.png';
            }
          }
        }

        that.vehicle_speed = d4.last_speed;
        that.todays_odo = d4.today_odo;
        that.total_odo = d4.total_odo;
        that.last_ping_on = d4.last_ping_on;
        that.allData.marker[d4._id].setIcon(that.allData.marker[d4._id].dicon);
        if (that.ongoingMoveMarker[d4._id])
          clearTimeout(that.ongoingMoveMarker[d4._id]);
        if (that.ongoingGoToPoint[d4._id])
          clearTimeout(that.ongoingGoToPoint[d4._id]);
        that.allData.marker[d4._id].setPosition(dd2);
        // debugger
        that.liveTrack(that.allData.map, that.allData.marker[d4._id], [dd1], 50, 10, d4._id)

      }
      // if (d4 != undefined)
      //   (function (data) {

      //     if (data == undefined) {
      //       return;
      //     }

      //     if (data._id != undefined && data.last_location != undefined) {
      //       var key = data._id;
      //       let ic = _.cloneDeep(that.icons[data.iconType]);
      //       if (!ic) {
      //         return;
      //       }
      //       ic.path = null;
      //       if (moment().diff(moment(data.last_ping_on), 'minutes') < 60) {
      //         if (that.plt.is('ios')) {
      //           ic.url = 'www/assets/imgs/vehicles/runningbus.png';
      //         } else {
      //           if (that.plt.is('android')) {
      //             ic.url = './assets/imgs/vehicles/runningbus.png';
      //           } else {
      //             ic.url = './assets/imgs/vehicles/runningbus.png';
      //           }
      //         }
      //       }
      //       else {
      //         if (that.plt.is('ios')) {
      //           ic.url = 'www/assets/imgs/vehicles/stoppedbus.png';
      //         } else {
      //           if (that.plt.is('android')) {
      //             ic.url = './assets/imgs/vehicles/stoppedbus.png';
      //           } else {
      //             ic.url = './assets/imgs/vehicles/stoppedbus.png';
      //           }
      //         }
      //       }
      //       that.vehicle_speed = data.last_speed;
      //       that.todays_odo = data.today_odo;
      //       that.total_odo = data.total_odo;
      //       that.last_ping_on = data.last_ping_on;

      //       if (that.allData[key]) {
      //         that.socketSwitch[key] = data;
      //         that.allData[key].mark.setIcon(ic);
      //         that.allData[key].mark.setPosition(that.allData[key].poly[1]);
      //         var temp = _.cloneDeep(that.allData[key].poly[1]);
      //         that.allData[key].poly[0] = _.cloneDeep(temp);
      //         that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

      //         // var speed = data.status == "RUNNING" ? data.last_speed : 0;
      //         that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, 50, 10, center, data._id, that.elementRef, data.iconType, data.status, that);
      //       }
      //       else {
      //         that.allData[key] = {};
      //         that.socketSwitch[key] = data;
      //         that.allData[key].poly = [];

      //         that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

      //         if (data.last_location != undefined) {

      //           that.allData.map.addMarker({
      //             title: data.Device_Name,
      //             position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
      //             icon: ic,
      //           }).then((marker: Marker) => {

      //             that.allData[key].mark = marker;
      //             if (that.selectedVehicle == undefined) {
      //               marker.showInfoWindow();
      //             }
      //             if (that.selectedVehicle != undefined) {
      //               marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
      //                 .subscribe(e => {
      //                   that.liveVehicleName = data.Device_Name;

      //                   that.onClickShow = true;
      //                 });
      //             }
      //             // var speed = data.status == "RUNNING" ? data.last_speed : 0;
      //             // that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, 50, 10, center, data._id, that.elementRef, data.iconType, data.status, that);
      //           })
      //         }
      //       }
      //     }
      //   })(d4)
    })
  }

  liveTrack(map, mark, coords, speed, delay, id) {
    let that = this;
    var target = 0;

    // clearTimeout(that.ongoingGoToPoint[id]);
    // clearTimeout(that.ongoingMoveMarker[id]);

    // if (center) {
    //   map.setCameraTarget(coords[0]);
    // }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(new LatLng(lat, lng));
          }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(dest);
          }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  // liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

  //   var target = 0;

  //   clearTimeout(that.ongoingGoToPoint[id]);
  //   clearTimeout(that.ongoingMoveMarker[id]);

  //   if (center) {
  //     map.setCameraTarget(coords[0]);
  //   }

  //   function _goToPoint() {
  //     if (target > coords.length)
  //       return;

  //     var lat = mark.getPosition().lat;
  //     var lng = mark.getPosition().lng;

  //     var step = (speed * 1000 * delay) / 3600000; // in meters
  //     if (coords[target] == undefined)
  //       return;

  //     var dest = new LatLng(coords[target].lat, coords[target].lng);
  //     var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
  //     var numStep = distance / step;
  //     var i = 0;
  //     var deltaLat = (coords[target].lat - lat) / numStep;
  //     var deltaLng = (coords[target].lng - lng) / numStep;

  //     function changeMarker(mark, deg) {
  //       mark.setRotation(deg);
  //       mark.setIcon(icons);
  //     }
  //     function _moveMarker() {

  //       lat += deltaLat;
  //       lng += deltaLng;
  //       i += step;
  //       var head;
  //       if (i < distance) {
  //         head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
  //         head = head < 0 ? (360 + head) : head;
  //         if ((head != 0) || (head == NaN)) {
  //           changeMarker(mark, head);
  //         }
  //         if (that.selectedVehicle != undefined) {
  //           map.setCameraTarget(new LatLng(lat, lng));
  //         }
  //         that.latlngCenter = new LatLng(lat, lng);
  //         mark.setPosition(new LatLng(lat, lng));
  //         that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
  //       }
  //       else {
  //         head = Spherical.computeHeading(mark.getPosition(), dest);
  //         head = head < 0 ? (360 + head) : head;
  //         if ((head != 0) || (head == NaN)) {
  //           changeMarker(mark, head);
  //         }
  //         if (that.selectedVehicle != undefined) {
  //           map.setCameraTarget(dest);
  //         }
  //         that.latlngCenter = dest;
  //         mark.setPosition(dest);
  //         target++;
  //         that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
  //       }
  //     }
  //     _moveMarker();
  //   }
  //   _goToPoint();
  // }

  signleVehLiveTrack(data) {
    let that = this;
    that.liveDataShare = data;
    that.deviceDeatils = data;
    that.onClickShow = false;
    that.mapData1 = [];
    this.trackRouteID = undefined;

    if (that.allData.map != undefined) {
      that.allData.map.remove();
    }

    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that.allData = {};
    that.socketChnl = [];
    that.socketSwitch = {};

    let styles = [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ebe3cd"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#523735"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f1e6"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#c9b2a6"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#dcd2be"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ae9e90"
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#93817c"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#a5b076"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#447530"
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffeb3b"
          },
          {
            "weight": 1
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.icon",
        "stylers": [
          {
            "color": "#ff6841"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ff6841"
          },
          {
            "weight": 1
          }
        ]
      },
      {
        "featureType": "poi.school",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#fff9fb"
          },
          {
            "weight": 8
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f1e6"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#fdfcf8"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f8c967"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#e9bc62"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e98d58"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#db8555"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#806b63"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#8f7d77"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#ebe3cd"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dfd2ae"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#b9d3c2"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#92998d"
          }
        ]
      }
    ];
    if (data) {
      if (data.last_location) {

        let mapOptions = {
          backgroundColor: 'white',
          controls: {
            compass: true,
            zoom: true,
            mapToolbar: true
          },
          gestures: {
            rotate: false,
            tilt: false
          },
          styles: styles
        }
        let map = GoogleMaps.create('map_canvas', mapOptions);
        map.animateCamera({
          target: { lat: 20.5937, lng: 78.9629 },
          zoom: 15,
          duration: 1000,
          padding: 0 // default = 20px
        });
        map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        that.allData.map = map;
        that.allData.marker = [];
        that.socketInit(data);
      } else {
        that.allData.map = that.newMap();
        that.allData.marker = [];
        that.socketInit(data);
      }

      if (that.selectedVehicle != undefined) {
        that.onClickShow = true;
      }
    }
    that.showBtn = true;
  }

  getProfilestudent() {
    var payload = { "createdByUser": { "_eval": "Id", "value": this.userdetails._id } }
    this.apiCall.getProfilestudentApi(payload)
      .subscribe(data => {
        let that = this;
        var icurl;
        if (that.plt.is('ios')) {
          icurl = 'www/assets/imgs/pin_school_alt.png';
        } else {
          if (that.plt.is('android')) {
            icurl = './assets/imgs/pin_school_alt.png';
          } else {
            icurl = './assets/imgs/pin_school_alt.png';
          }
        }
        for (var i = 0; i < data.length; i++) {
          that.allData.map.addMarker({
            title: data[i].schoolName,
            icon: icurl,
            position: {
              lat: data[i].latLng.lat,
              lng: data[i].latLng.lng
            }
          }).then((marker: Marker) => {
            that.centerPonitLat = data[i].latLng.lat;
            that.centerPointLng = data[i].latLng.lng;
          });
        }
      }, err => {
        console.log(err);
      })
  }

  showRoute() {
    let that = this;
    if (that.deviceDeatils) {
      debugger;
      if (that.deviceDeatils.scheduleschools.length > 0) {
        for (var w = 0; w < that.deviceDeatils.scheduleschools.length; w++) {
          that.trackRouteID = that.deviceDeatils.scheduleschools[w].trackRoute;
          that.getTrackAPI(that.trackRouteID);
        }
      }
    }
  }

  getTrackAPI(trackid) {
    this.apiCall.getTrackRoute(trackid, this.userdetails._id)
      .subscribe(resp => {
        let that = this;
        (function (data) {

          if (data == undefined) {
            return;
          }
          that.routeData = data;

          that.mapData1 = [];
          for (var i = 0; i < that.routeData.routePath.length; i++) {
            that.mapData1.push({ "lat": that.routeData.routePath[i].location.coordinates[1], "lng": that.routeData.routePath[i].location.coordinates[0] });
          }
          that.locations = [];

          for (var i = 0; i < data.poi.length; i++) {
            var arr = {
              lat: that.routeData.poi[i].poi.location.coordinates[1],
              lng: that.routeData.poi[i].poi.location.coordinates[0],
              stopName: that.routeData.poi[i].poi.poiname,
              sbUsers: that.routeData.poi[i].sbUsers[0]
            };
            that.locations.push(arr);
          }
          var iconurl;
          var parkicon;
          if (that.plt.is('ios')) {
            iconurl = 'www/assets/imgs/flag.png';
            parkicon = 'www/assets/imgs/park.png';
          } else {
            if (that.plt.is('android')) {
              iconurl = './assets/imgs/flag.png';
              parkicon = './assets/imgs/park.png';
            } else {
              iconurl = './assets/imgs/flag.png';
              parkicon = './assets/imgs/park.png';
            }
          }
          for (var i = 0; i < that.locations.length; i++) {

            if (that.locations[i].sbUsers = that.userdetails._id) {
              that.allData.map.addMarker({
                title: that.locations[i].stopName,
                position: that.locations[i],
                icon: iconurl,
              }).then((marker: Marker) => {
                marker.showInfoWindow();
              });
            } else {
              that.allData.map.addMarker({
                title: that.locations[i].stopName,
                position: that.locations[i],
                icon: parkicon
              }).then((marker: Marker) => {
                marker.showInfoWindow();
              });
            }
          }
          console.log("latlang.............", that.mapData1)
          that.allData.map.addPolyline({
            points: that.mapData1,
            color: '#42adf4',
            width: 5,
            geodesic: true
          })

        })(resp)

      },
        err => {
          console.log("we got error: ", err)
        })
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        that.mapHideTraffic = !that.mapHideTraffic;
        if (that.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      }
    }
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: 60 // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = "http://139.59.50.225/share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }
}