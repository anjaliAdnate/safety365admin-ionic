import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { GroupModel } from './group-model/group-model';
// import { GroupModalPage } from '../customers/modals/group-modal/group-modal';
// import { UpdateGroup } from './update-group/update-group';


@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage implements OnInit {

  groupslist: any;
  groupData: any;
  GroupArray: any[];
  islogin: any;
  allGroupName: any;
  allGroup: any;
  status: any;
  Groupdevice: any;
  groupId: any;
  datetime: any;

  toastCtrl: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apigroupcall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("_id=> " + this.islogin._id);
  }

  ngOnInit() {
    this.getgroup();
  }

  getgroup() {
    console.log("getgroup");
    var baseURLp = 'http://139.59.50.225/groups/getGroups_list?uid=' + this.islogin._id;
    this.apigroupcall.startLoading().present();
    this.apigroupcall.getGroupCall(baseURLp)
      .subscribe(data => {
        this.apigroupcall.stopLoading();

        this.groupData = data;
        this.allGroup = data["group_details"]
        console.log("GroupData=> " + JSON.stringify(this.allGroup));
        // console.log("customerlist=> ", this.customerslist)
        this.GroupArray = [];

        for (var i = 0; i < this.allGroup.length; i++) {
          this.allGroupName = this.allGroup[i].name;
          this.datetime = this.allGroup[i].last_modified;
          this.Groupdevice = this.allGroup[i].devices.length;
          this.status = this.allGroup[i].status;
          this.groupId = this.allGroup[i]._id;
          this.GroupArray.push({ 'groupname': this.allGroupName, 'status': this.status, 'vehicle': this.Groupdevice, '_id': this.groupId, 'datetime': this.datetime })
          console.log(this.GroupArray);
        }
      },
        err => {
          this.apigroupcall.stopLoading();
          console.log("error found=> " + err);
        });
  }


  deleteItem(item) {
    let that = this;
    console.log("delete")
    console.log(item)
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          console.log(item._id)
          that.deleteDevice(item._id);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  deleteDevice(d_id) {
    this.apigroupcall.startLoading().present();
    this.apigroupcall.deleteGroupCall(d_id)
      .subscribe(data => {
        this.apigroupcall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: 'Vehicle deleted successfully!',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.navCtrl.push(GroupsPage);
        });

        toast.present();
      },
        err => {
          this.apigroupcall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }



  openAddGroupModal() {
    let modal = this.modalCtrl.create('GroupModalPage');
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.getgroup();
    })
    modal.present();
  }


  openUpdateGroupModal() {
    let modal = this.modalCtrl.create('UpdateGroup');
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.getgroup();
    })
    modal.present();
  }


}
