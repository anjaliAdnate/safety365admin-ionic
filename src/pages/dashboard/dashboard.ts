import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform } from 'ionic-angular';
import { Chart } from 'chart.js';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import '../../assets/js/chartjs-plugin-labels.min.js';
import * as io from 'socket.io-client';   // with ES6 import
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit {
  public cartCount: number = 0;

  islogin: any;
  uuid: string;
  PushToken: string;
  dealerStatus: any;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  from: any;
  to: any;
  TotalDevice: any[];
  devices: any;
  Running: any;
  IDLING: any;
  Stopped: any;
  Maintance: any;
  OutOffReach: any;
  NogpsDevice: any;
  totalVechiles: any;
  devices1243: any[];
  isdevice: string;
  socket: any;
  token: string;
  totaldevices: any;
  // pushnotify: any;
  // drawerOptions: any;
  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private push: Push,
    public plt: Platform) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    ///////////////////////////////
    this.events.publish('user:updated', this.islogin);
    /////////////////////////////////


    this.token = localStorage.getItem("DEVICE_TOKEN");
    console.log("device token dashboard=> ", this.token)
    // console.log("islogin=> " + JSON.stringify(this.islogin));
    this.uuid = localStorage.getItem('uuid');
    // console.log("uuid=> " + this.uuid);
    this.PushToken = localStorage.getItem('PushToken');
    // console.log("pushtoken=> " + this.PushToken);
    this.dealerStatus = this.islogin.isDealer;
    // console.log("dealer status => " + this.dealerStatus);

    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();
    // console.log("form=> " + this.from);

    ///////////////=======Push notifications========////////////////
    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });

    this.socket = io('http://139.59.50.225/notifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected tabs');
      console.log("socket connected tabs", this.socket.connected)
    });

    this.socket.on(this.islogin._id, (msg) => {
      // this.notData.push(msg);
      this.cartCount += 1;
      // console.log("tab notice data=> " + this.notData)
    })
    ///////////////=======Push notifications========////////////////
  }

  ngOnInit() {
    this.getDashboarddevices();

    localStorage.removeItem("LiveDevice");
  }

  doRefresh(refresher) {
    this.getDashboarddevices();
    refresher.complete();
  }

  seeNotifications() {
    this.navCtrl.push('AllNotificationsPage');
  }

  getChart() {
    let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });

    // if the chart is not undefined (e.g. it has been created)
    // then destory the old one so we can create a new one later
    if (this.doughnutChart) {
      this.doughnutChart.destroy();
    }
    // var theHelp = Chart.helpers;
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["RUNNING", "IDLING", "STOPPED", "MAINTENANCE", "OUT OF REACH", "NO GPS"],
        datasets: [{
          label: '# of Votes',
          data: that.TotalDevice,
          backgroundColor: [
            '#3CB371', '#ffc900', '#FF6384', 'gray', '#92f5eb', '#d27d7d'
          ],
          hoverBackgroundColor: [
            'rgba(51, 255, 153, 0.7)',
            'rgba(255, 255, 51, 0.7)',
            'rgba(255, 99, 132, 0.7)',
            'rgba(204, 204, 179, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(255, 102, 51, 0.7)'
          ],
          // borderColor: ['black', 'black', 'black', 'black', 'black', 'black'],
          // borderWidth: [1.5, 1.5, 1.5, 1.5, 1.5, 1.5]
        }]
      },
      options: {
        plugins: {
          labels: {
            render: 'value',
            fontSize: 16,
            fontColor: '#fff',
            images: [
              {
                src: 'image.png',
                width: 16,
                height: 16
              }
            ],
            outsidePadding: 2,
            textMargin: 4
          }
        },
        elements: {
          center: {
            text: that.totaldevices,
            color: '#5694ce', // Default is #000000
            fontStyle: 'Arial', // Default is Arial
            sidePadding: 10 // Defualt is 20 (as a percentage)
          }
        },
        tooltips: {
          enabled: false
        },
        pieceLabel: {
          mode: 'value',
          fontColor: ['white', 'white', 'white', 'white', 'white']
        },
        responsive: true,
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 8,
            boxWidth: 10,
            usePointStyle: true
          },
          position: 'bottom',
        },
        onClick: function (event, elementsAtEvent) {
          var activePoints = elementsAtEvent;
          if (activePoints[0]) {
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];
            var label = chartData.labels[idx];
            var value = chartData.datasets[0].data[idx];
            localStorage.setItem('status', label);
            that.navCtrl.push('AddDevicesPage', {
              label: label,
              value: value
            });
          }
        }
      }
    });
  }

  historyDevice() {
    localStorage.setItem("MainHistory", "MainHistory");
    this.navCtrl.push('HistoryDevicePage');
  }

  opennotify() {
    this.navCtrl.push('AllNotificationsPage')
  }

  addPushNotify() {

    let that = this;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    if (that.token != null) {
      var os;
      if (that.plt.is('ios')) {
        os = "ios";
      } else {
        if(that.plt.is('android')) {
          os = "android";
        }
      }
      var pushdata = {
        "uid": that.islogin._id,
        "token": that.token,
        "os": os
      }
      // console.log("pushdata=> " + JSON.stringify(pushdata));
      that.apiCall.pushnotifyCall(pushdata)
        .subscribe(data => { },
          error => {
            console.log(error);
          });
    } else {
      that.pushSetup();
    }

  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        // icon: './assets/imgs/yellow-bus-icon-40028.png'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);
    pushObject.on('registration')
      .subscribe((registration: any) => {
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
      });

    pushObject.on('error').subscribe(error => {
    });
  }

  getDashboarddevices() {
    console.log("getdevices");
    this.apiCall.startLoading().present();
    this.apiCall.dashboardcall(this.islogin.email, this.from, this.to, this.islogin._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.totaldevices = data.Total_Vech
        console.log("devices => " + JSON.stringify(this.totaldevices));
        ///// add push token to server //////////////
        this.addPushNotify();
        /////////////////////////////////////////////
        // debugger;
        this.TotalDevice = [];
        this.devices = data;
        // console.log("devices => " + JSON.stringify(this.devices));

        this.Running = this.devices.running_devices;
        this.TotalDevice.push(this.Running)
        this.IDLING = this.devices["Ideal Devices"]
        this.TotalDevice.push(this.IDLING);
        this.Stopped = this.devices["OFF Devices"]
        this.TotalDevice.push(this.Stopped);
        this.Maintance = this.devices["Maintance Device"]
        this.TotalDevice.push(this.Maintance);
        this.OutOffReach = this.devices["OutOfReach"]
        this.TotalDevice.push(this.OutOffReach);
        this.NogpsDevice = this.devices["no_gps_fix_devices"]
        this.TotalDevice.push(this.NogpsDevice);
        // console.log("total devices => " + this.TotalDevice);
        this.totalVechiles = this.Running + this.IDLING + this.Stopped + this.Maintance + this.OutOffReach + this.NogpsDevice;
        // console.log(this.devices.off_ids);
        this.getChart();

      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getStopdevices() {
    console.log("getStopdevices");
    this.apiCall.startLoading().present();
    this.apiCall.stoppedDevices(this.islogin._id, this.islogin.email, this.devices.off_ids)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices = data;
        // console.log("devices 1 => " + this.devices);
        this.devices1243 = [];
        this.devices = data.devices.reverse();
        this.devices1243.push(data);
        // console.log("devices1243=> " + this.devices1243);

        this.devices = this.devices;
        // console.log(this.devices);
        localStorage.setItem('devices', this.devices);
        this.isdevice = localStorage.getItem('devices');

        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
        console.log(this.islogin);
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  vehiclelist() {
    console.log("coming soon");
    this.navCtrl.push('AddDevicesPage');
  }

  live() {
    this.navCtrl.push('LivePage');
  }

  geofence() {
    this.navCtrl.push('GeofencePage');
  }

}
