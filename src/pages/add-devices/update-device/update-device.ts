import { Component, OnInit } from "@angular/core";
import { ViewController, NavParams, ToastController, AlertController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
import { ApiServiceProvider } from "../../../providers/api-service/api-service";

@IonicPage()
@Component({
    selector: 'page-update-device',
    templateUrl: './update-device.html'
})
export class UpdateDevicePage implements OnInit {
    updatevehForm: FormGroup;
    vehData: any;
    selectUser: any;
    allVehicle: any;
    deviceModel: any;
    allGroup: any;
    allGroupName: any;
    islogin: any;
    submitAttempt: boolean;
    groupstaus: any;
    userdata: any;
    modeldata: any;
    vehType: any;
    groupstaus_id: any;
    devicedetail: any = {};
    vehicleType: any;
    modeldata_id: any;
    userdata_id: any;
    eDate: any;
    vehicleType_id: any;
    currentYear: any;
    minDate: any;

    constructor(
        public apiCall: ApiServiceProvider,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        navPar: NavParams,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController
    ) {

        this.vehData = navPar.get("vehData");
        console.log("vehicle data=> ", this.vehData);

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        if (this.vehData.expiration_date == null) {
            this.eDate = this.currentYear;
        } else {
            this.eDate = moment(this.vehData.expiration_date).format('YYYY-MM-DD');
        }
        var d_type, d_user, v_type, g_name;
        if (this.vehData.device_model != undefined) {
            d_type = this.vehData.device_model['device_type']
        } else {
            d_type = '';
        }
        if (this.vehData.user != undefined) {
            d_user = this.vehData.user['first_name'];
        } else {
            d_user = '';
        }

        if (this.vehData.vehicleType != undefined) {
            v_type = this.vehData.vehicleType['brand'];
        } else {
            v_type = '';
        }

        if (this.vehData.vehicleGroup != undefined) {
            g_name = this.vehData.vehicleGroup['name'];
        } else {
            g_name = '';
        }

        // console.log("device model=> ", this.vehData.device_model['device_type'])
        // console.log("user => ", this.vehData.user['first_name'])
        // console.log("brand => ", this.vehData.vehicleType['brand'])
        // console.log("device model=> ", this.vehData.device_model['device_type'])
        
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"))
        // =============== end

        this.updatevehForm = formBuilder.group({
            device_name: [this.vehData.Device_Name, Validators.required],
            device_id: [this.vehData.Device_ID],
            driver: [this.vehData.driver_name],
            sim_number: [this.vehData.sim_number, Validators.required],
            contact_number: [this.vehData.contact_number, Validators],
            SpeedLimit: [this.vehData.SpeedLimit, Validators.required],
            ExipreDate: [this.eDate, Validators.required],
            gName: [g_name],
            device_type: [d_type],
            first_name: [d_user],
            brand: [v_type]
        });
        console.log("date format=> " + moment(this.vehData.expiration_date).format('YYYY-MM-DD'))
        // this.updatevehForm.patchValue({
        //     ExipreDate: moment(this.vehData.expiration_date).format('YYYY-MM-DD')
        // })

    }

    ngOnInit() {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    updateDevices() {
        let that = this;
        that.submitAttempt = true;
        if (that.updatevehForm.valid) {
            ///////////////////////////
            // debugger;
            console.log("form valuess=> ", that.updatevehForm.value)
            if (that.updatevehForm.value.device_type != '') {
                if (that.vehData.device_model != null || that.vehData.device_model != undefined) {
                    if (that.updatevehForm.value.device_type == that.vehData.device_model['device_type']) {
                        that.modeldata_id = that.vehData.device_model['_id']
                    } else {
                        that.modeldata_id = that.modeldata._id;
                    }
                } else {
                    if (that.modeldata._id != undefined) {
                        that.modeldata_id = that.modeldata._id;
                    } else {
                        that.modeldata_id = null;
                    }

                }

            } else {
                that.modeldata_id = null;
            }
            if (that.updatevehForm.value.gName != '') {
                if (that.vehData.vehicleGroup != null || that.vehData.vehicleGroup != undefined) {
                    if (that.updatevehForm.value.gName == that.vehData.vehicleGroup['name']) {
                        that.groupstaus_id = that.vehData.vehicleGroup['_id'];
                    } else {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                } else {
                    if (that.groupstaus._id != undefined) {
                        that.groupstaus_id = that.groupstaus._id;
                    } else {
                        that.groupstaus_id = null;
                    }
                }

            } else {
                that.groupstaus_id = null;
            }
            if (that.updatevehForm.value.first_name != '') {
                if (that.vehData.user != null || that.vehData.user != undefined) {
                    if (that.updatevehForm.value.first_name == that.vehData.user['first_name']) {
                        that.userdata_id = that.vehData.user['_id'];
                    } else {
                        that.userdata_id = that.userdata._id;
                    }
                } else {
                    if (that.userdata._id != undefined) {
                        that.userdata_id = that.userdata._id;
                    } else {
                        that.userdata_id = null;
                    }
                }

            } else {
                that.userdata_id = null;
            }
            if (that.updatevehForm.value.brand != '') {
                if (that.vehData.vehicleType != null || that.vehData.vehicleType != undefined) {
                    if (that.updatevehForm.value.brand == that.vehData.vehicleType['brand']) {
                        that.vehicleType_id = that.vehData.vehicleType['_id'];
                    } else {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                } else {
                    if (that.vehicleType._id != undefined) {
                        that.vehicleType_id = that.vehicleType._id;
                    } else {
                        that.vehicleType_id = null;
                    }
                }

            } else {
                that.vehicleType_id = null;
            }

            ///////////////////////////

            that.devicedetail = {
                "_id": that.vehData._id,
                "devicename": that.updatevehForm.value.device_name,
                "drname": that.updatevehForm.value.driver,
                "sim": that.updatevehForm.value.sim_number,
                "iconType": null,
                "dphone": that.updatevehForm.value.contact_number,
                "speed": that.updatevehForm.value.SpeedLimit,
                "vehicleGroup": that.groupstaus_id,
                "device_model": that.modeldata_id,
                "expiration_date": new Date(that.updatevehForm.value.ExipreDate).toISOString(),
                "user": that.userdata_id,
                "vehicleType": that.vehicleType_id
            }

            console.log("all details=> " + that.devicedetail);
            that.apiCall.startLoading().present();
            that.apiCall.deviceupdateCall(that.devicedetail)
                .subscribe(data => {
                    that.apiCall.stopLoading();
                    var editdata = data;
                    console.log("editdata=> " + editdata);
                    let toast = that.toastCtrl.create({
                        message: editdata.message,
                        position: 'top',
                        duration: 2000
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        that.viewCtrl.dismiss(editdata);
                    });

                    toast.present();
                },
                    err => {
                        that.apiCall.stopLoading();
                        var body = err._body;
                        var msg = JSON.parse(body);
                        let alert = that.alerCtrl.create({
                            title: 'Oops!',
                            message: msg.message,
                            buttons: ['OK']
                        });
                        alert.present();
                    });
        }
    }

    deviceModelata(deviceModel) {
        console.log("deviceModel" + deviceModel);
        this.modeldata = deviceModel;
        console.log("modal data device_type=> " + this.modeldata.device_type);
    }

    GroupStatusdata(status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    }

    userselectData(userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    }

    vehicleTypeselectData(vehicletype) {
        console.log(vehicletype);
        this.vehicleType = vehicletype;
        console.log("vehType=> " + this.vehicleType._id);
    }

    getGroup() {
        console.log("get group");
        var baseURLp = 'http://139.59.50.225/groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this.allGroup = data;
                this.allGroup = data["group details"]
                // console.log("all group=> " + this.allGroup[0].name);
                for (var i = 0; i < this.allGroup.length; i++) {
                    this.allGroupName = this.allGroup[i].name;
                    // console.log("allGroupName=> "+this.allGroupName);
                }
                console.log("allGroupName=> " + this.allGroupName)
            },
                err => {
                    console.log(err);
                    this.apiCall.stopLoading();
                });
    }

    getDeviceModel() {
        console.log("getdevices");
        var baseURLp = 'http://139.59.50.225/deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(data => {
                this.deviceModel = data;
                console.log("selected user=> ", this.deviceModel);
            },
                err => {
                    console.log(err);
                });
    }

    getSelectUser() {
        console.log("get user");

        var baseURLp = 'http://139.59.50.225/users/getAllUsers?dealer=' + this.islogin._id;

        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(data => {
                this.selectUser = data;
                console.log("selected user=> ", this.selectUser);
            },
                error => {
                    console.log(error)
                });
    }

    getVehicleType() {
        console.log("get getVehicleType");

        var baseURLp = 'http://139.59.50.225/vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(data => {
                // this.apiCall.stopLoading();
                this.allVehicle = data;
                console.log("all vehicles=> ", this.allVehicle)
            }, err => {
                console.log(err);
                // this.apiCall.stopLoading();
            });

    }
}