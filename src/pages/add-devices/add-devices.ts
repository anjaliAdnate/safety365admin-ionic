import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController, Navbar } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SMS } from '@ionic-native/sms';
// import { ImmobilizePage } from './immobilize/immobilize';
declare var google;

@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {

  islogin: any;
  isDealer: any;
  islogindealer: string;
  stausdevice: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any = [];
  allDevicesSearch: any = [];

  socket: any;
  isdevice: string;
  userPermission: string;
  option_switch: boolean = false;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  @ViewChild(Navbar) navBar: Navbar;
  page: number = 0;
  limit: number = 5;
  ndata: any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];
  deivceId: any;
  immobType: any;
  respMsg: any;
  commandStatus: any;
  intervalID: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private sms: SMS,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.isDealer = this.islogin.isDealer
    console.log("isDealer=> " + this.isDealer);

    this.islogindealer = localStorage.getItem('isDealervalue');
    console.log("islogindealer=> " + this.islogindealer);

    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
    } else {
      // localStorage.removeItem("status");
      this.stausdevice = undefined;
    }
  }

  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }
    this.getdevices();
    console.log(this.isDealer);
  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "the server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.navCtrl.setRoot("DashboardPage");
        }
      }]
    });
    alerttemp.present();
  }

  showVehicleDetails(vdata) {
    this.navCtrl.push('VehicleDetailsPage', {
      param: vdata
    });
  }

  doRefresh(refresher) {
    let that = this;
    that.page = 0;
    that.limit = 5;
    console.log('Begin async operation', refresher);
    this.getdevices();
    refresher.complete();
  }

  shareVehicle(d_data) {
    let that = this;
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      inputs: [
        {
          name: 'device_name',
          value: d_data.Device_Name
        },
        {
          name: 'shareId',
          placeholder: 'Enter Email Id/Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Share',
          handler: (data) => {
            that.sharedevices(data, d_data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data, d_data) {
    let that = this;
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  showDeleteBtn(b) {
    let that = this;
    if (b) {
      var u = b.split(",");
      for (let p = 0; p < u.length; p++) {
        if (that.islogin._id == u[p]) {
          return true;
        }
      }
    }
    else {
      return false;
    }
  }

  sharedVehicleDelete(device) {
    let that = this;
    that.deivceId = device;
    let alert = that.alertCtrl.create({
      message: 'Do you want to delete this share vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          that.removeDevice(that.deivceId._id);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(data => {
        console.log(data)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'Shared Device was deleted successfully!',
          duration: 1500
        });
        toast.onDidDismiss(() => {
          this.getdevices();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  showSharedBtn(a, b) {
    // debugger
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevice) {
        baseURLp = 'http://139.59.50.225/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
      }
      else {
        baseURLp = 'http://139.59.50.225/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
      }
      that.ndata = [];
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(
          res => {
            that.ndata = res.devices;
            for (let i = 0; i < that.ndata.length; i++) {
              that.allDevices.push(that.ndata[i]);
            }
            that.allDevicesSearch = that.allDevices;
          },
          error => {
            console.log(error);
          });
      infiniteScroll.complete();
    }, 500);
  }

  getdevices() {
    var baseURLp;
    if (this.stausdevice) {
      baseURLp = 'http://139.59.50.225/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = 'http://139.59.50.225/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.ndata = data.devices;
        this.allDevices = this.ndata;
        this.allDevicesSearch = this.ndata;
        let that = this;
        that.userPermission = localStorage.getItem('condition_chk');
        if (that.userPermission == 'true') {
          that.option_switch = true;
        } else {
          if (that.userPermission == 'false') {
            that.option_switch = false;
          }
        }
      },
        err => {
          console.log("error=> ", err);
          this.apiCall.stopLoading();
          if (err.message == "Timeout has occurred") {
            this.timeoutAlert();
          }
        });
  }

  livetrack(device) {
    localStorage.setItem("LiveDevice", "LiveDevice");
    this.navCtrl.push('LiveSingleDevice', {
      device: device
    })
  }

  showHistoryDetail(device) {
    this.navCtrl.push('HistoryDevicePage', {
      device: device
    })
  }

  device_address(device, index) {
    let that = this;

    that.allDevices[index].address = "N/A";
    if (!device.last_location) {
      that.allDevices[index].address = "N/A";
    } else if (device.last_location) {
      var lattitude = device.last_location.lat;
      var longitude = device.last_location.long;

      var geocoder = new google.maps.Geocoder;
      var latlng = new google.maps.LatLng(lattitude, longitude);

      var request = {
        "latLng": latlng
      };

      geocoder.geocode(request, function (resp, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (resp[0] != null) {
            that.allDevices[index].address = resp[0].formatted_address;
          } else {
            console.log("No address available")
          }
        }
        else {
          that.allDevices[index].address = 'N/A';
        }
      })
    }
  }

  callSearch(ev) {
    console.log(ev.target.value)
    var searchKey = ev.target.value;
    // this.apiCall.startLoading().present();
    this.apiCall.callSearchService(this.islogin.email, this.islogin._id, searchKey)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        console.log("search result=> " + data)
        this.allDevicesSearch = data.devices;
        this.allDevices = data.devices;
      })
  }

  IgnitionOnOff(d) {
    this.messages = undefined;
    this.dataEngine = d;

    var baseURLp = 'http://139.59.50.225/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;

    this.apiCall.startLoading().present();
    this.apiCall.ignitionoffCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        // debugger;
        this.DeviceConfigStatus = data;
        this.immobType = data[0].imobliser_type;

        // console.log("DeviceConfigStatus=>   " + JSON.stringify(this.DeviceConfigStatus))
        if (this.dataEngine.ignitionLock == '1') {
          this.messages = 'Do you want to unlock the engine?'
        } else {
          if (this.dataEngine.ignitionLock == '0') {
            this.messages = 'Do you want to lock the engine?'
          }
        }

        // let profileModal = this.modalCtrl.create('ImmobilizePage', { param: d, udata: data, msg: this.messages });
        // profileModal.present();

        let alert = this.alertCtrl.create({
          message: this.messages,
          buttons: [{
            text: 'YES',
            handler: () => {
              // debugger;
              if (this.immobType == 0 || this.immobType == undefined) {
                var devicedetail = {
                  "_id": this.dataEngine._id,
                  "engine_status": !this.dataEngine.engine_status
                }
                this.apiCall.startLoading().present();
                this.apiCall.deviceupdateCall(devicedetail)
                  .subscribe(response => {
                    this.apiCall.stopLoading();
                    this.editdata = response;
                    const toast = this.toastCtrl.create({
                      message: response.message,
                      duration: 2000,
                      position: 'top'
                    });
                    toast.present();
                    this.responseMessage = "Edit successfully";
                    this.getdevices();

                    var msg;
                    if (!this.dataEngine.engine_status) {
                      msg = this.DeviceConfigStatus[0].resume_command;
                    }
                    else {
                      msg = this.DeviceConfigStatus[0].immoblizer_command;
                    }

                    this.sms.send(d.sim_number, msg);
                    const toast1 = this.toastCtrl.create({
                      message: 'SMS sent successfully',
                      duration: 2000,
                      position: 'bottom'
                    });
                    toast1.present();
                  },
                    error => {
                      this.apiCall.stopLoading();
                      console.log(error);
                    });
              } else {
                // call othere service
                console.log("Call server code here!!")
                // debugger;
                var data = {
                  "imei": d.Device_ID,
                  "_id": this.dataEngine._id,
                  "engine_status": d.ignitionLock,
                  "protocol_type": d.device_model.device_type
                }
                this.apiCall.startLoading().present();
                this.apiCall.serverLevelonoff(data)
                  .subscribe(resp => {
                    this.apiCall.stopLoading();
                    console.log("ignition on off=> ", resp)
                    this.respMsg = resp;

                    this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
                    this.intervalID = setInterval(() => {
                      this.apiCall.callResponse(this.respMsg._id)
                        .subscribe(data => {
                          console.log("interval=> " + data)
                          this.commandStatus = data.status;

                          if (this.commandStatus == 'SUCCESS') {
                            clearInterval(this.intervalID);
                            this.apiCall.stopLoading();
                            const toast1 = this.toastCtrl.create({
                              message: 'process has completed successfully!',
                              duration: 1500,
                              position: 'bottom'
                            });
                            toast1.present();
                            this.getdevices();
                          }
                        })
                    }, 1000);

                    // this.getdevices();

                  },
                    err => {
                      this.apiCall.stopLoading();
                      console.log("error in onoff=>", err);
                      if (err.message == "Timeout has occurred") {
                        this.timeoutAlert();
                      }
                    });
              }
            }
          },
          {
            text: 'No'
          }]
        });
        alert.present();
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });

    // debugger;


  };

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  getItems(ev: any) {
    console.log(ev.target.value, this.allDevices)
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.allDevicesSearch);
  }

  onClear(ev) {
    // debugger;
    this.getdevices();
    ev.target.value = '';
  }

  openAdddeviceModal() {
    let profileModal = this.modalCtrl.create('AddDeviceModalPage');
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.getdevices();
    });
    profileModal.present();
  }
}


@Component({
  template: `
  
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;Edit
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
      <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;Delete
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
      <ion-icon name="share"></ion-icon>&nbsp;&nbsp;Share
      </ion-item>
    </ion-list>
  `
})
export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController
  ) {
    this.vehData = navParams.get("vehData");
    console.log("popover data=> ", this.vehData);

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() {
    // if (this.navParams.data) {
    //   this.contentEle = this.navParams.data.contentEle;
    //   this.textEle = this.navParams.data.textEle;

    // }
  }

  editItem() {
    console.log("edit")
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.viewCtrl.dismiss();
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    console.log("delete")
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: 'Vehicle deleted successfully!',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          // this.navCtrl.push(AddDevicesPage);
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: 'Enter Email Id/Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Share',
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data) {
    let that = this;
    console.log(data.shareId);
    var devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        var editdata = data;
        console.log(editdata);
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        // toast.onDidDismiss(() => {
        //   console.log('Dismissed toast');

        // });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }
}
